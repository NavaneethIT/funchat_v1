// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebaseConfig: {
        // apiKey: "AIzaSyBd0wQXQdj4W97Qsn83r3w-FM32lZQscY0",
        // authDomain: "funchat-d6ac8.firebaseapp.com",
        // databaseURL: "https://funchat-d6ac8-default-rtdb.firebaseio.com",
        // projectId: "funchat-d6ac8",
        // storageBucket: "funchat-d6ac8.appspot.com",
        // messagingSenderId: "29191080521",
        // appId: "1:29191080521:web:70b857213a3dbb18878d98"

        // apiKey: "AIzaSyBRG10mmcUSdtd0OZQPk85_NsIMXOcrNo4",
        // authDomain: "funchat-v1.firebaseapp.com",
        // databaseURL: "https://funchat-v1-default-rtdb.firebaseio.com",
        // projectId: "funchat-v1",
        // storageBucket: "funchat-v1.appspot.com",
        // messagingSenderId: "896411201443",
        // appId: "1:896411201443:web:e3d0c990d0a524155d7416"

        // apiKey: "AIzaSyC81UZsQE0T3Te8W_-srgsMWHAv7S6ROyk",
        // authDomain: "funchatting-96d0f.firebaseapp.com",
        // databaseURL: "https://funchatting-96d0f-default-rtdb.firebaseio.com",
        // projectId: "funchatting-96d0f",
        // storageBucket: "funchatting-96d0f.appspot.com",
        // messagingSenderId: "423092244954",
        // appId: "1:423092244954:web:85f80046814fc6d17a30e3"

        // apiKey: "AIzaSyDqkHchY0-YBwu_W9mjuisjwkxJuCuYinY",
        // authDomain: "funchat-05-12-2023.firebaseapp.com",
        // databaseURL: "https://funchat-05-12-2023-default-rtdb.firebaseio.com",
        // projectId: "funchat-05-12-2023",
        // storageBucket: "funchat-05-12-2023.appspot.com",
        // messagingSenderId: "934537553138",
        // appId: "1:934537553138:web:969d2dd35d200c2efcdc6b"


        apiKey: "AIzaSyBVcbyYqoJJko1AVNxV-NJpNO044qP6xrE",
        authDomain: "funchat-v1-06-dec-2024.firebaseapp.com",
        databaseURL: "https://funchat-v1-06-dec-2024-default-rtdb.firebaseio.com",
        projectId: "funchat-v1-06-dec-2024",
        storageBucket: "funchat-v1-06-dec-2024.firebasestorage.app",
        messagingSenderId: "413421987560",
        appId: "1:413421987560:web:b6239a9b86367f00a70b5d"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
