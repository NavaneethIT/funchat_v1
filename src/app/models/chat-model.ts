export interface UserData {
    uid: string;
    displayName: string;
    aboutStatus: any;
    email: any;
    photoURL: any;
    friendsList: FriendsListModel;
    isGroup: boolean
    chatId: any;
}

export interface FriendsListModel extends Array<{
    uid: string;
    chatId: string;
    displayName: string;
    aboutStatus: any;
    email: any;
    photoURL: any;
}> { }
