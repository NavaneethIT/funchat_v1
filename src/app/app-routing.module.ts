import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { canActivate, redirectLoggedInTo, redirectUnauthorizedTo } from '@angular/fire/auth-guard';

const redirectUnauthorizedToSignIn = () => redirectUnauthorizedTo(['/']);
const redirectSignInToChat = () => redirectLoggedInTo(['/tabs']);

const routes: Routes = [{
    path: '',
    loadChildren: () => import('./pages/landing/landing.module').then(m => m.LandingPageModule),
    // ...canActivate(redirectSignInToChat)
}, {
    path: 'signup',
    loadChildren: () => import('./pages/signup/signup.module').then(m => m.SignupPageModule)
}, {
    path: 'signin',
    loadChildren: () => import('./pages/signin/signin.module').then(m => m.SigninPageModule),
    // ...canActivate(redirectSignInToChat)
}, {
    path: 'forgot',
    loadChildren: () => import('./pages/forgot/forgot.module').then(m => m.ForgotPageModule)
}, {
    path: 'tabs',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule),
    // ...canActivate(redirectUnauthorizedToSignIn)
}, {
    path: 'profile',
    loadChildren: () => import('./pages/profile/profile.module').then(m => m.ProfilePageModule)
}, {
    path: 'chats',
    loadChildren: () => import('./pages/chats/chats.module').then(m => m.ChatsPageModule)
}, {
    path: 'userlist',
    loadChildren: () => import('./pages/userlist/userlist.module').then(m => m.UserlistPageModule)
}, {
    path: 'creategroup',
    loadChildren: () => import('./pages/creategroup/creategroup.module').then(m => m.CreategroupPageModule)
}, {
    path: 'commonprofile',
    loadChildren: () => import('./pages/commonprofile/commonprofile.module').then(m => m.CommonProfilePageModule)
}, {
    path: 'videoaudiocall',
    loadChildren: () => import('./pages/videoaudiocall/videoaudiocall.module').then(m => m.VideoaudiocallPageModule)
}];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            preloadingStrategy: PreloadAllModules,
            onSameUrlNavigation: 'reload'
        })
    ],
    exports: [RouterModule]
})

export class AppRoutingModule { }
