import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { initializeApp } from 'firebase/app';
import { provideFirebaseApp } from '@angular/fire/app';
import { provideAuth, getAuth } from '@angular/fire/auth';
import { provideFirestore, getFirestore } from '@angular/fire/firestore';
import { provideStorage, getStorage } from '@angular/fire/storage';
import { environment } from '../environments/environment';
import { ChatService } from './services/chat.service';
import { Storage } from '@ionic/storage';
// import { WebRTCService } from './services/webrtc.service';
// import { WebRTCConfig } from './services/webrtc.config';
import { AngularFireModule } from '@angular/fire/compat'; // Import AngularFireModule
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    // provideFirebaseApp(() => initializeApp(environment.firebaseConfig)),  // Initialize Firebase
    AngularFireModule.initializeApp(environment.firebaseConfig),  // Initialize AngularFire
    AngularFireAuthModule,
    AngularFirestoreModule
  ],
  providers: [
    {
      provide: RouteReuseStrategy,
      useClass: IonicRouteStrategy,
    },
    ChatService,
    Storage,
    // WebRTCService,
    // WebRTCConfig
    provideFirebaseApp(() => initializeApp(environment.firebaseConfig)),
    provideAuth(() => getAuth()), // Provide Firebase Auth
    provideFirestore(() => getFirestore()), // Provide Firestore
    provideStorage(() => getStorage()),
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
