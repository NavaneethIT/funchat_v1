import { Component, OnInit } from '@angular/core';
import { QueryDocumentSnapshot } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AlertController, LoadingController, PopoverController } from '@ionic/angular';
import { error } from 'protractor';
import { Observable, Subscription } from 'rxjs';
import { FriendsListModel, UserData } from 'src/app/models/chat-model';
import { ChatService } from 'src/app/services/chat.service';
import { ProfileCardPopoverComponent } from 'src/app/shared/profile-card-popover';

@Component({
    selector: 'app-chatlist',
    templateUrl: './chatlist.page.html',
    styleUrls: ['./chatlist.page.scss'],
    standalone: false
})

export class ChatlistPage implements OnInit {

    public chatGroupFriendList: FriendsListModel = [];
    public dupChatGroupFriendList: FriendsListModel = [];
    public getCurrentUser: any;
    public loading: HTMLIonLoadingElement;
    public mySubscription: Subscription;
    public isAllUser: boolean = true;

    constructor(
        private router: Router,
        private chatService: ChatService,
        private loadingController: LoadingController,
        private popoverCtrl: PopoverController
    ) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.searchChatUserList();
    }

    async ngOnInit() {
        this.loading = await this.loadingController.create({
            message: 'Loading...',
            backdropDismiss: false,
            keyboardClose: false
        });
        await this.loading.present();
        this.getChatsList();
    }

    public getChatsList() {
        this.chatService.mergeFriendAndGroupList().subscribe((res: any) => {
            this.loading.dismiss();
            this.chatGroupFriendList = [];
            this.dupChatGroupFriendList = [];
            if (res?.length > 0) {
                this.isAllUser = true;
                this.chatGroupFriendList = res;
                this.dupChatGroupFriendList = res;
            } else {
                this.isAllUser = false;
            }
        }, (error: any) => {
            this.isAllUser = false;
            this.chatGroupFriendList = [];
            this.dupChatGroupFriendList = [];
            this.loading.dismiss();
        });
    }

    public sortChatList(a: any, b: any) {
        return b.lastMessageAt - a.lastMessageAt;
    }

    public searchChatUserList() {
        this.mySubscription = this.chatService.getSearchText().subscribe((res: any) => {
            this.chatGroupFriendList = this.dupChatGroupFriendList;
            if (res) {
                if (res.trim() == '') {
                    return;
                }
                this.chatGroupFriendList = this.chatGroupFriendList.filter((ele: any) => {
                    if ((ele.displayName.toLowerCase().indexOf(res.toLowerCase())) > -1) {
                        return true;
                    }
                    return false;
                });
            }
        });
    }

    public chatClick(event: any, getUser: any) {
        localStorage.setItem('chatUser', JSON.stringify(getUser));
        this.router.navigateByUrl('/chats', { replaceUrl: true });
    }

    public async showProfileCardPopOver(event: any, user: any) {
        const popover = await this.popoverCtrl.create({
            component: ProfileCardPopoverComponent,
            componentProps: {
                chatUser: user
            },
            cssClass: 'profile-card-class'
        });
        await popover.present();
    }

    ngOnDestroy() {
        this.mySubscription.unsubscribe();
    }

}
