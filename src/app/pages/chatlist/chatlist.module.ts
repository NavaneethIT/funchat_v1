import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ChatlistPageRoutingModule } from './chatlist-routing.module';
import { ChatlistPage } from './chatlist.page';
import { ProfileCardPopoverComponent } from 'src/app/shared/profile-card-popover';
import { DatetransformPipe } from 'src/app/shared/datetransform.pipe';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ChatlistPageRoutingModule
    ],
    declarations: [
        ChatlistPage,
        ProfileCardPopoverComponent,
        DatetransformPipe,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class ChatlistPageModule { }
