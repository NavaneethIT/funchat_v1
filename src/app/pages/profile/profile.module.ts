import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilePageRoutingModule } from './profile-routing.module';

import { ProfilePage } from './profile.page';
import { ScrollbarThemeModule } from 'src/app/services/scrollbarDirective';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ProfilePageRoutingModule,
        ScrollbarThemeModule
    ],
    declarations: [
        ProfilePage,
    ]
})

export class ProfilePageModule { }
