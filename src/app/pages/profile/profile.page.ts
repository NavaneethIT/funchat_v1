import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { ChatService } from 'src/app/services/chat.service';
import { ActionSheetController, AlertController, LoadingController, PopoverController } from '@ionic/angular';
import { ProfileCardPopoverComponent } from 'src/app/shared/profile-card-popover';
import { AccessPopoverComponent } from 'src/app/shared/access-popover';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss'],
})

export class ProfilePage implements OnInit {

    // public task: AngularFireUploadTask;
    public profileImage: string;
    public getCurrentUser: any;
    public getFriendsList: any = [];
    public getGroupsList: any = [];

    constructor(
        public router: Router,
        // public storage: AngularFireStorage,
        public loadingController: LoadingController,
        private alertController: AlertController,
        private popoverCtrl: PopoverController,
        public actionSheetController: ActionSheetController,
        public chatService: ChatService,
    ) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.chatService.getGroupList().subscribe((res: any) => {
            this.getGroupsList = res;
        });

        if (this.getCurrentUser?.photoURL === null || this.getCurrentUser?.photoURL === undefined || this.getCurrentUser?.photoURL === "null") {
            this.profileImage = "../../../assets/icon/user_logo.png";
        } else {
            this.profileImage = this.getCurrentUser?.photoURL;
        }

        this.getFriendsList = (this.getCurrentUser?.friendsList?.length > 0) ? this.getCurrentUser?.friendsList : [];
    }

    ngOnInit() {
    }

    public backClick() {
        this.router.navigateByUrl('/tabs', { replaceUrl: true });
    }

    public async cameraClick() {
        const actionSheet = await this.actionSheetController.create({
            header: 'Photos',
            cssClass: 'my-custom-class',
            buttons: [{
                text: 'From Photos',
                handler: () => {
                    this.callGalleryCamera(CameraSource.Photos);
                }
            }, {
                text: 'Take Picture',
                handler: () => {
                    this.callGalleryCamera(CameraSource.Camera);
                }
            }, {
                text: 'Cancel',
                role: 'cancel',
                handler: () => { }
            }]
        });
        await actionSheet.present();
    }

    public base64ToImage(dataURI: any) {
        const fileDate = dataURI.split(',');
        const byteString = atob(fileDate[1]);
        const arrayBuffer = new ArrayBuffer(byteString.length);
        const int8Array = new Uint8Array(arrayBuffer);
        for (let i = 0; i < byteString.length; i++) {
            int8Array[i] = byteString.charCodeAt(i);
        }
        const blob = new Blob([arrayBuffer], { type: 'image/png' });
        return blob;
    }

    public async callGalleryCamera(getCameraSource: any) {
        const loading = await this.loadingController.create({
            message: 'Loading...',
            backdropDismiss: false,
            keyboardClose: false
        });

        const image = await Camera.getPhoto({
            quality: 100,
            allowEditing: true,
            resultType: CameraResultType.DataUrl,
            source: getCameraSource,
            saveToGallery: true,
            promptLabelCancel: 'Cancel',
        }).then(async (getPhoto) => {
            await loading.present();
            const file: any = this.base64ToImage(getPhoto.dataUrl);
            const filePath = `profileImages/${this.getCurrentUser?.uid}`;
            const customMetadata = { app: 'Profile Pictures' };
            // const fileRef = this.storage.ref(filePath);
            // this.storage.upload(filePath, file, { customMetadata }).snapshotChanges().pipe().subscribe((res) => {
            //     if (res?.state === 'success') {
            //         fileRef.getDownloadURL().subscribe(async (resp) => {
            //             if (resp.length > 0) {
            //                 const postData = {
            //                     uid: this.getCurrentUser?.uid,
            //                     url: resp,
            //                     friendsList: this.getCurrentUser?.friendsList
            //                 };
            //                 this.chatService.updateProfilePic(postData).then((res: any) => {
            //                     loading.dismiss();
            //                     if (res) {
            //                         this.profileImage = resp;
            //                         const getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
            //                         getCurrentUser.photoURL = '';
            //                         getCurrentUser.photoURL = resp;
            //                         localStorage.setItem('currentUser', JSON.stringify(getCurrentUser));
            //                     }
            //                 }, async (er) => {
            //                     loading.dismiss();
            //                     const alert = await this.alertController.create({
            //                         header: 'Upload Failed',
            //                         message: er.message,
            //                         buttons: ['ok']
            //                     });
            //                     await alert.present();
            //                 });
            //             }
            //         }, async (error) => {
            //             loading.dismiss();
            //             const alert = await this.alertController.create({
            //                 header: 'Upload Failed',
            //                 message: error.message,
            //                 buttons: ['ok']
            //             });
            //             await alert.present();
            //         });
            //     }
            // });
        }).catch(async (er) => {
            loading.dismiss();
            const alert = await this.alertController.create({
                header: 'Cancelled',
                message: er.message,
                buttons: ['ok']
            });
            await alert.present();
        });
    }

    public async showProfileCardPopOver(user: any) {
        if (user.uid !== this.getCurrentUser.uid) {
            const popover = await this.popoverCtrl.create({
                component: ProfileCardPopoverComponent,
                componentProps: {
                    chatUser: user
                },
                cssClass: 'profile-card-class'
            });
            await popover.present();
        }
    }

    public commonGroupChatClick(getGroup: any) {
        localStorage.setItem('chatUser', JSON.stringify(getGroup));
        this.router.navigateByUrl('/chats', { replaceUrl: true });
    }

    public async chatClick(getUser: any) {
        const currentUserInfo = JSON.parse(localStorage.getItem('currentUser'));
        const popover = await this.popoverCtrl.create({
            component: AccessPopoverComponent,
            componentProps: {
                chatUser: getUser,
                isAdmin: false
            },
            cssClass: 'access-popover-class'
        });
        await popover.present();
    }

    public async signOutClick() {
        const alert = await this.alertController.create({
            header: 'Sign Out',
            message: 'Do you want to Sign Out ?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => { }
                }, {
                    text: 'Yes',
                    handler: () => {
                        this.chatService.signOut().then((res: any) => {
                            localStorage.removeItem('currentUser');
                            localStorage.removeItem('chatUser');
                            localStorage.removeItem('chatID');
                            localStorage.removeItem('groupList');
                            localStorage.removeItem('chatUser');
                            this.router.navigateByUrl('/signin', { replaceUrl: true });
                        }, async (error) => {
                            const errorAlert = await this.alertController.create({
                                header: 'Sign Out Failed',
                                message: error.message
                            });
                            await errorAlert.present();
                        });
                    }
                }
            ]
        });
        await alert.present();
    }
}
