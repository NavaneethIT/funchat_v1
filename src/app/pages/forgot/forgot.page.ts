import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { ChatService } from 'src/app/services/chat.service';

@Component({
    selector: 'app-forgot',
    templateUrl: './forgot.page.html',
    styleUrls: ['./forgot.page.scss'],
})

export class ForgotPage implements OnInit {

    public credentialForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private chatService: ChatService,
        private alertController: AlertController,
        private loadingController: LoadingController
    ) { }

    ngOnInit() {
        this.credentialForm = this.fb.group({
            email: ['', [Validators.required, Validators.email]]
        });
    }
    
    public async forgotClick() {
        const loading = await this.loadingController.create({
            message: 'Loading...',
            backdropDismiss: false,
            keyboardClose: false
        });
        await loading.present();

        this.chatService.forgetPassword(this.credentialForm.value).then(async (res) => {
            loading.dismiss();
            if (res) {
                this.alertController.create({
                    header: 'Forgot Password',
                    message: 'Email Sent to your registered EmailID please follow the instructions to reset your password',
                    buttons: [{
                        text: 'Ok',
                        handler: () => {
                            this.router.navigateByUrl('signin', { replaceUrl: true });
                        }
                    }]
                }).then((res: any) => {
                    res.present();
                });
            }
        }).catch(async (error: any) => {
            loading.dismiss();
            this.alertController.create({
                header: 'Forgot Password',
                message: error.message,
                buttons: [{
                    text: 'Ok',
                    handler: () => {
                        this.router.navigateByUrl('signin', { replaceUrl: true });
                    }
                }]
            }).then((res: any) => {
                res.present();
            });
        });
    }

    public signUpClick() {
        this.router.navigateByUrl('signup', { replaceUrl: true });
    }

    public signInClick() {
        this.router.navigateByUrl('signin', { replaceUrl: true });
    }

    get email() {
        return this.credentialForm.get('email');
    }

}
