import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { ChatService } from 'src/app/services/chat.service';

@Component({
    selector: 'app-userlist',
    templateUrl: './userlist.page.html',
    styleUrls: ['./userlist.page.scss'],
    standalone: false
})

export class UserlistPage implements OnInit {

    public usersList = [];
    public dupUsersList = [];
    public isAllUser: boolean = true;
    public getCurrentUser: any;
    public isShowSearch: boolean = false;
    public searchText: string = '';

    constructor(
        private router: Router,
        private chatService: ChatService,
        private loadingController: LoadingController
    ) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {
        this.getAllUserData();
    }

    public backClick() {
        this.router.navigateByUrl('/tabs', { replaceUrl: true });
    }

    public searchClick() {
        this.isShowSearch = !this.isShowSearch;
    }

    public searchUser(event: any) {
        this.usersList = this.dupUsersList;
        if (event.trim() == '') {
            return;
        }
        this.usersList = this.usersList.filter((ele: any) => {
            if ((ele.displayName.toLowerCase().indexOf(event.toLowerCase())) > -1) {
                return true;
            }
            return false;
        });
    }

    public searchClear(event: any) {
        this.searchText = '';
        this.usersList = this.dupUsersList;
    }

    public async getAllUserData() {
        const loading = await this.loadingController.create({
            message: 'Loading...',
            backdropDismiss: false,
            keyboardClose: false
        });
        await loading.present();
        this.isAllUser = true;
        this.usersList = [];
        this.dupUsersList = [];
        this.chatService.getAllUser().subscribe((res: any) => {
            loading.dismiss();
            console.log(res)
            if (res) {
                this.isAllUser = true;
                res = res.filter((ele: any) => ele.uid !== this.getCurrentUser.uid).map((el: any) => {
                    console.log(el);
                    const newData = {
                        uid: el.uid,
                        displayName: el.displayName,
                        aboutStatus: el.aboutStatus,
                        email: el.email,
                        photoURL: el.photoURL,
                        isGroup: el.isGroup,
                        createdAt: el.createdAt
                    };
                    return newData;
                }).sort((a: any, b: any) => {
                    return (a.displayName > b.displayName) ? 1 : ((b.displayName > a.displayName) ? -1 : 0);
                });
                if (res?.length > 0) {
                    this.isAllUser = true;
                    this.usersList = res;
                    this.dupUsersList = res;
                } else {
                    this.isAllUser = false;
                    this.usersList = [];
                    this.dupUsersList = [];
                }
            } else {
                this.isAllUser = false;
                this.usersList = [];
                this.dupUsersList = [];
            }
        }, (error: any) =>{
            loading.dismiss();
            this.isAllUser = false;
        });
    }

    public chatClick(getUser: any) {
        localStorage.setItem('chatUser', JSON.stringify(getUser));
        this.router.navigateByUrl('/chats', { replaceUrl: true });
    }

    public newGroupChatClick() {
        this.router.navigateByUrl('/creategroup', { replaceUrl: true });
    }

}
