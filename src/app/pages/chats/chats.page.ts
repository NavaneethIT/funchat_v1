import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { ActionSheetController, AlertController, IonContent, LoadingController, ModalController, PopoverController, ToastController } from '@ionic/angular';
import { FriendsListModel, UserData } from 'src/app/models/chat-model';
import { BehaviorSubject, Subscription } from 'rxjs';
import { ProfilePopoverComponent } from '../../shared/profile-popover';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { VideoaudiocallPage } from '../videoaudiocall/videoaudiocall.page';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AngularFireStorage } from '@angular/fire/compat/storage';

@Component({
    selector: 'app-chats',
    templateUrl: './chats.page.html',
    styleUrls: ['./chats.page.scss'],
    standalone: false
})

export class ChatsPage implements OnInit {

    @ViewChild('content') content: IonContent;
    @ViewChild('myInput') myInput: ElementRef;


    public getCurrentUser: UserData;
    public chatUserData: UserData;
    public isFirstMsg: boolean = false;
    public newMessage = '';
    public chatMessage = [];
    public isShowSearch: boolean = false;
    public dupChatMessage: any = [];
    public searchText: string = '';
    public mySubscription: Subscription;

    constructor(
        private router: Router,
        private toastController: ToastController,
        private popoverCtrl: PopoverController,
        private alertController: AlertController,
        private loadingController: LoadingController,
        private actionSheetController: ActionSheetController,
        private chatService: ChatService,
        private element: ElementRef,
        private afs: AngularFirestore,
        private storage: AngularFireStorage,
        private modalCtrl: ModalController
    ) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
        const getChatData = JSON.parse(localStorage.getItem('chatUser'));
        this.chatUserData = getChatData;
        if (!getChatData.hasOwnProperty('isGroup')) {
            getChatData.isGroup = false;
            this.chatUserData = getChatData;
            this.refreshChatData();
        } else {
            this.refreshChatData();
        }
    }

    ngOnInit() {
    }

    async init() {
        // this.router.navigateByUrl(`videoaudiocall`, { replaceUrl: true });
        // const modal = await this.modalCtrl.create({
        //     component: VideoaudiocallPage
        // });
        // return await modal.present();
    }

    ngAfterContentChecked() {
        const getIosStyle: HTMLDivElement = document.querySelector('ion-title.profileNameClass.ios.title-default.hydrated')?.shadowRoot.querySelector('.toolbar-title') as HTMLDivElement;
        const getFooterIosStyle: HTMLDivElement = document.querySelector('ion-toolbar.ios.in-toolbar.hydrated.toolbar-interactive.toolbar-textarea.toolbar-input.toolbar-has-placeholder')?.shadowRoot.querySelector('.toolbar-background') as HTMLDivElement;

        if (getIosStyle) {
            getIosStyle.style.width = 'auto';
            getIosStyle.style.margin = '0px -20%';
        }

        if (getFooterIosStyle) {
            getFooterIosStyle.style.borderStyle = 'none';
        }
    }

    public resizeTextArea(): void {
        let textArea: HTMLTextAreaElement;
        let ionTextArea: HTMLTextAreaElement;
        if (textArea == null) {
            ionTextArea = this.element.nativeElement.getElementsByTagName('ion-textarea')['myInput'];
            textArea = this.element.nativeElement.getElementsByTagName('textarea')[0];
        }
        textArea.style.height = 'auto';
        textArea.rows = 1;
        ionTextArea.querySelector('.attachClass').setAttribute('style', 'margin: 5% 0px -20% 0px');

        if (textArea.scrollHeight <= 40) {
            textArea.style.height = 40 + 'px';
            ionTextArea.classList.add('message-input');
            ionTextArea.classList.remove('message-input-br');
            ionTextArea.querySelector('.attachClass').setAttribute('style', 'margin: 5% 0px -20% 0px');
        } else if (textArea.scrollHeight > 40 && textArea.scrollHeight < 100) {
            textArea.style.height = textArea.scrollHeight + 'px';
            ionTextArea.classList.remove('message-input');
            ionTextArea.classList.add('message-input-br');
            ionTextArea.querySelector('.attachClass').setAttribute('style', 'margin: 18% 0px -20% 0px');
        } else {
            textArea.style.height = 100 + 'px';
            ionTextArea.classList.remove('message-input');
            ionTextArea.classList.add('message-input-br');
            ionTextArea.querySelector('.attachClass').setAttribute('style', 'margin: 35% 0px -20% 0px');
        }
    }

    public refreshChatData() {
        if (!this.chatUserData?.isGroup) {
            const getCurrentChatData = this.getCurrentUser?.friendsList.find((ele: FriendsListModel[0]) => ele.uid === this.chatUserData.uid);
            if (getCurrentChatData && getCurrentChatData?.chatId) {
                this.isFirstMsg = true;
                localStorage.removeItem('chatUser');
                localStorage.setItem('chatUser', JSON.stringify(getCurrentChatData));
                localStorage.setItem('chatID', getCurrentChatData.chatId);
                this.getNewMessages(getCurrentChatData.chatId);
            } else {
                localStorage.removeItem('chatID');
                localStorage.setItem('chatUser', JSON.stringify(this.chatUserData));
            }
        } else {
            localStorage.setItem('chatID', this.chatUserData.chatId);
            this.getNewMessages(this.chatUserData.chatId);
        }

        this.mySubscription = this.chatService.getProfileSearch().subscribe((resp: boolean) => {
            this.isShowSearch = resp;
        });
    }

    public backClick() {
        localStorage.removeItem('chatUser');
        localStorage.removeItem('chatID');
        this.router.navigateByUrl('/tabs', { replaceUrl: true });
    }

    public async showPopover(event: any) {
        const popover = await this.popoverCtrl.create({
            component: ProfilePopoverComponent,
            componentProps: {
                chatUser: this.chatUserData
            },
            cssClass: 'myPopOverClass'
        });
        await popover.present();
    }

    public viewFriendGroupProfile() {
        localStorage.setItem('viewFrom', JSON.stringify('page'));
        this.router.navigateByUrl('/commonprofile', { replaceUrl: true });
    }

    public sendMessage(isGroup: boolean, isImage: boolean, getChatId: any) {
        if (
            this.newMessage !== '' &&
            this.newMessage !== null &&
            this.newMessage !== undefined
        ) {
            if (isGroup) {
                const postData = {
                    uid: this.getCurrentUser?.uid,
                    userName: this.getCurrentUser?.displayName,
                    content: this.newMessage,
                    dummyMsg: '',
                    receiverUid: '',
                    receiverName: '',
                    createdAt: Date.now(),
                    isFirst: false,
                    isGroup: true,
                    isDummy: false,
                    isAdminMsg: false,
                    isImage: isImage
                };
                this.chatService.addNewGroupMessage(postData).then((res) => {
                    this.scrollTo();
                    this.newMessage = '';
                });
                this.newMessage = '';
            } else {
                getChatId = getChatId ? getChatId : localStorage.getItem('chatID') ? localStorage.getItem('chatID') : this.afs.createId();
                this.chatService.addNewMessage(this.newMessage, this.chatUserData, isImage, getChatId).then((res) => {
                    this.scrollTo();
                    this.newMessage = '';
                    if (!this.isFirstMsg) {
                        this.isFirstMsg = true;
                        const getChatId = localStorage.getItem('chatID');
                        this.getNewMessages(getChatId);
                    }
                });
                this.newMessage = '';
            }
        }
    }

    public async cameraClick(isGroup: boolean) {
        const actionSheet = await this.actionSheetController.create({
            header: 'Photos',
            cssClass: 'my-custom-class',
            buttons: [{
                text: 'From Photos',
                handler: () => {
                    this.callGalleryCamera(CameraSource.Photos, isGroup);
                }
            }, {
                text: 'Take Picture',
                handler: () => {
                    this.callGalleryCamera(CameraSource.Camera, isGroup);
                }
            }, {
                text: 'Cancel',
                role: 'cancel',
                handler: () => { }
            }]
        });
        await actionSheet.present();
    }

    public base64ToImage(dataURI: any) {
        const fileDate = dataURI.split(',');
        const byteString = atob(fileDate[1]);
        const arrayBuffer = new ArrayBuffer(byteString.length);
        const int8Array = new Uint8Array(arrayBuffer);
        for (let i = 0; i < byteString.length; i++) {
            int8Array[i] = byteString.charCodeAt(i);
        }
        const blob = new Blob([arrayBuffer], { type: 'image/png' });
        return blob;
    }

    public async callGalleryCamera(getCameraSource: any, isGroup: boolean) {
        const loading = await this.loadingController.create({
            message: 'Loading...',
            backdropDismiss: false,
            keyboardClose: false
        });

        const image = await Camera.getPhoto({
            quality: 100,
            allowEditing: true,
            resultType: CameraResultType.DataUrl,
            source: getCameraSource,
            saveToGallery: true
        }).then(async (getPhoto) => {
            await loading.present();
            const chatID = '';
            localStorage.getItem('chatID') ? localStorage.getItem('chatID') : this.afs.createId();
            const fileName = Date.now();
            const file: any = this.base64ToImage(getPhoto.dataUrl);
            const filePath = `chatImages/${chatID}/${fileName}`;
            const customMetadata = { app: 'Group Profile Pictures' };
            const fileRef = this.storage.ref(filePath);
            this.storage.upload(filePath, file, { customMetadata }).snapshotChanges().pipe().subscribe((res) => {
                if (res?.state === 'success') {
                    fileRef.getDownloadURL().subscribe(async (resp) => {
                        if (resp.length > 0) {
                            this.newMessage = resp;
                            this.sendMessage(isGroup, true, chatID);
                            loading.dismiss();
                        }
                    }, async (error) => {
                        loading.dismiss();
                        const alert = await this.alertController.create({
                            header: 'Upload Failed',
                            message: error.message,
                            buttons: ['ok']
                        });
                        await alert.present();
                    });
                }
            });
        }).catch(async (er) => {
            loading.dismiss();
            const alert = await this.alertController.create({
                header: 'Cancelled',
                message: er.message,
                buttons: ['ok']
            });
            await alert.present();
        });
    }

    public getNewMessages(getChatId?: any) {
        this.chatMessage = [];
        this.dupChatMessage = [];
        this.chatService.getMessages(getChatId).subscribe((res: any) => {
            this.chatMessage = [];
            this.dupChatMessage = [];
            this.chatMessage = res;
            this.dupChatMessage = res;
            this.scrollTo();
        });
    }

    public searchMsgCancelClick(value: any) {
        this.searchText = '';
        this.isShowSearch = false;
        this.chatMessage = this.dupChatMessage;
        this.scrollTo();
        this.chatService.setProfileSearch(this.isShowSearch);
    }

    public searchMsgClearClick(event: any) {
        this.searchText = '';
        this.isShowSearch = false;
        this.chatMessage = this.dupChatMessage;
        this.scrollTo();
        this.chatService.setProfileSearch(this.isShowSearch);
    }

    public searchMsgData(event: any) {
        this.chatMessage = this.dupChatMessage;
        if (event.trim() == '') {
            return;
        }
        this.chatMessage = this.chatMessage.filter((ele: any) => {
            if ((ele.content.toLowerCase().indexOf(event.toLowerCase())) > -1) {
                return true;
            }
            return false;
        });
    }

    public scrollTo() {
        setTimeout(() => {
            this.content.scrollToBottom();
        }, 300)
    }

    public async errorToast() {
        const toast = await this.toastController.create({
            message: 'Add some message to send',
            duration: 1500,
            color: 'danger',
            animated: true,
            translucent: true,
            position: 'bottom'
        });
        toast.present();
    }

    ngOnDestory() {
        this.mySubscription.unsubscribe();
    }
}
