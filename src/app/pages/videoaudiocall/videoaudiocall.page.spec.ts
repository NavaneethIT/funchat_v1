import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { VideoaudiocallPage } from './videoaudiocall.page';

describe('VideoaudiocallPage', () => {
    let component: VideoaudiocallPage;
    let fixture: ComponentFixture<VideoaudiocallPage>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [VideoaudiocallPage],
            imports: [IonicModule.forRoot()]
        }).compileComponents();

        fixture = TestBed.createComponent(VideoaudiocallPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
