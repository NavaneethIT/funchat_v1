import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VideoaudiocallPage } from './videoaudiocall.page';

const routes: Routes = [
    {
        path: '',
        component: VideoaudiocallPage
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class VideoaudiocallPageRoutingModule { }
