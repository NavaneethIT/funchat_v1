import { Component, ElementRef, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
// import Peer from 'peerjs';
import { ChatService } from 'src/app/services/chat.service';
// import { WebRTCService } from 'src/app/services/webrtc.service';

@Component({
    selector: 'app-videoaudiocall',
    templateUrl: './videoaudiocall.page.html',
    styleUrls: ['./videoaudiocall.page.scss'],
})

export class VideoaudiocallPage implements OnInit {

    public getCurrentUser: any;
    public topVideoFrame = 'partner-video';
    public userId: string;
    public partnerId: string;
    public myEl: HTMLMediaElement;
    public partnerEl: HTMLMediaElement;
    getChatUser: any;
    lazyStream: MediaStream;
    peer: any;
    peerList: any = [];
    currentPeer: any;
    peerId: any;
    peerIdShare: string;


    myVideo: HTMLMediaElement;
    otherVideo: HTMLMediaElement;
    
    me: any = {};
    otherUser: any;

    constructor(
        public chatService: ChatService,
        public elRef: ElementRef,
        // private webRTCService: WebRTCService,
        private nav: NavController,
        private modalCtrl: ModalController
    ) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.getChatUser = JSON.parse(localStorage.getItem('chatUser'));
        // this.peer = new Peer(this.getCurrentUser?.uid);
        // this.peer = new Peer();
    }

    ngOnInit() {
        // this.getPeerId();
        // this.myEl = this.elRef.nativeElement.querySelector('#my-video');


        // Find video elements
        this.myVideo = this.elRef.nativeElement.querySelector('#my-video');
        this.otherVideo = this.elRef.nativeElement.querySelector('#other-video');
        //
        // this.chatService.init(this.myVideo, this.otherVideo, () => {
        //     console.log('I\'m calling');
        // });
    }
    
    startCall() {
        // console.log('Call to ', this.getChatUser.uid);
        // this.chatService.call(this.getChatUser.uid);
    }
    
    stopCall() {
        // console.log('Stop calling to other user', this.getChatUser.uid);
        // this.chatService.endCall();
    }

















    // private getPeerId() {
    //     //Generate unique Peer Id for establishing connection
    //     // this.peer = new Peer(this.getCurrentUser?.uid);
    //     // const partnerID = new Peer(this.getChatUser?.uid);
    //     // this.peerIdShare = partnerID.id;
    //     this.peer.on('open', (id: any) => {
    //         console.log(id);
    //         // console.log(partnerID);
    //         this.peerId = id;
    //         // this.createURLToConnect(id);
    //     });

    //     // Peer event to accept incoming calls
    //     this.peer.on('call', (call) => {
    //         navigator.mediaDevices.getUserMedia({
    //             video: true,
    //             audio: true
    //         }).then((stream) => {
    //             this.lazyStream = stream;

    //             call.answer(stream);
    //             call.on('stream', (remoteStream) => {
    //                 if (!this.peerList.includes(call.peer)) {
    //                     this.streamRemoteVideo(remoteStream);
    //                     this.currentPeer = call.peerConnection;
    //                     this.peerList.push(call.peer);
    //                 }
    //             });

    //         }).catch(err => {
    //             console.log(err + 'Unable to get media');
    //         });
    //     });
    // }

    // connectWithPeer(): void {
    //     this.callPeer(this.peerIdShare);
    // }

    // private streamRemoteVideo(stream) {
    //     // console.log(stream);
    //     // this.myEl.srcObject = stream;
    //     const video = document.createElement('video');
    //     video.classList.add('video');
    //     video.srcObject = stream;
    //     video.play();

    //     document.getElementById('remote-video').append(video);
    // }

    // private callPeer(id: string): void {
    //     console.log(this.peer);
    //     navigator.mediaDevices.getUserMedia({
    //         video: true,
    //         audio: true
    //     }).then((stream) => {
    //         this.lazyStream = stream;

    //         const call = this.peer.call(id, stream);
    //         call.on('stream', (remoteStream) => {
    //             if (!this.peerList.includes(call.peer)) {
    //                 this.streamRemoteVideo(remoteStream);
    //                 this.currentPeer = call.peerConnection;
    //                 this.peerList.push(call.peer);
    //             }
    //         });
    //     }).catch(err => {
    //         console.log(err + 'Unable to connect');
    //     });
    // }

}
