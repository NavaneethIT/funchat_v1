import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { VideoaudiocallPageRoutingModule } from './videoaudiocall-routing.module';
import { VideoaudiocallPage } from './videoaudiocall.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        VideoaudiocallPageRoutingModule
    ],
    declarations: [VideoaudiocallPage]
})

export class VideoaudiocallPageModule { }
