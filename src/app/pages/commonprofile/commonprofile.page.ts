import { Component, OnInit } from '@angular/core';
// import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { NavigationEnd, Router } from '@angular/router';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { ActionSheetController, AlertController, LoadingController, PopoverController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { ChatService } from 'src/app/services/chat.service';
import { ProfileCardPopoverComponent } from 'src/app/shared/profile-card-popover';
import { AccessPopoverComponent } from 'src/app/shared/access-popover';

@Component({
    selector: 'app-common-profile',
    templateUrl: './commonprofile.page.html',
    styleUrls: ['./commonprofile.page.scss'],
})

export class CommonProfilePage implements OnInit {

    public isHeaderVisible: boolean = true;
    public usersList: any = [];
    public dupUsersList: any = [];
    public selecteGroupMembers: any = [];
    public alert: HTMLIonAlertElement;
    public chatUserDetails: any;
    // public task: AngularFireUploadTask;
    public getCurrentUser: any;
    public getalertOkBtn: HTMLElement;
    public createBy: any;
    public isShowMemberSearch: boolean = false;
    public searchMembersText: any = '';
    public getMembersList: any = [];
    public getDupMembersList: any = [];
    public getGroupList: any = [];
    public commonGroupList: any = [];
    public isAdmin: boolean = false;
    public navigationSubscription: Subscription;
    public editGroupOkBtn: HTMLElement;

    constructor(
        private router: Router,
        private alertController: AlertController,
        private loadingController: LoadingController,
        private actionSheetController: ActionSheetController,
        // private storage: AngularFireStorage,
        private popoverCtrl: PopoverController,
        private chatService: ChatService
    ) {
        this.navigationSubscription = this.router.events.subscribe((e: any) => {
            if (e instanceof NavigationEnd) {
                this.refreshData();
            }
        });
    }

    ngOnInit() {
    }

    public refreshData() {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.chatUserDetails = JSON.parse(localStorage.getItem('chatUser'));

        localStorage.removeItem('chatID');
        if (this.chatUserDetails?.chatId) {
            localStorage.setItem('chatID', this.chatUserDetails?.chatId);
        }

        if (this.chatUserDetails?.isGroup) {
            this.createBy = '';
            this.getMembersList = [];
            this.getDupMembersList = [];
            this.isAdmin = false;
            // this.chatService.getSingleGroupMembersList(this.chatUserDetails?.groupID).subscribe((gpMemRes: any) => {
            //     if (gpMemRes.payload.data() && gpMemRes.payload.data().groupMembers.length > 0) {
            //         this.chatUserDetails.groupMembers = [];
            //         this.chatUserDetails.groupMembers = gpMemRes.payload.data().groupMembers;
            //         this.getMembersList = gpMemRes.payload.data().groupMembers;
            //         this.getDupMembersList = gpMemRes.payload.data().groupMembers;
            //         const createdUser = this.getMembersList.find((ele: any) => ele.uid === this.getCurrentUser.uid);
            //         if (createdUser) {
            //             this.isAdmin = createdUser.isAdmin;
            //         }
            //         localStorage.removeItem('chatUser');
            //         localStorage.setItem('chatUser', JSON.stringify(this.chatUserDetails));
            //     }
            // }, (error) => {
            //     this.getMembersList = this.chatUserDetails?.groupMembers;
            //     this.getDupMembersList = this.chatUserDetails?.groupMembers;
            //     console.error(error);
            // });
            this.createBy = (this.chatUserDetails?.createdBy === this.getCurrentUser?.uid) ? 'You' : this.chatUserDetails?.createdName;
        } else {
            this.chatService.getGroupList().subscribe((res: any) => {
                this.getGroupList = [];
                this.commonGroupList = [];
                this.getGroupList = res;
                if (this.getGroupList?.length > 0) {
                    for (let i = 0; i < this.getGroupList?.length; i++) {
                        if (this.getGroupList[i].groupMembers?.length > 0) {
                            const getCommon = this.getGroupList[i].groupMembers.find((ele: any) => ele.uid === this.chatUserDetails?.uid);
                            if (getCommon) {
                                this.getGroupList[i].uid = this.getGroupList[i].groupID;
                                this.commonGroupList.push(this.getGroupList[i]);
                            }
                        }
                    }
                }
            });
        }
    }

    public backClick() {
        const res = JSON.parse(localStorage.getItem('viewFrom'));
        if (res === 'popup') {
            localStorage.removeItem('chatUser');
            localStorage.removeItem('chatID');
            this.router.navigateByUrl('/tabs', { replaceUrl: true });
        } else {
            this.router.navigateByUrl('/chats', { replaceUrl: true });
        }
        localStorage.removeItem('viewFrom');
    }

    public async editClick(getGroupDetails: any) {
        const actionSheet = await this.actionSheetController.create({
            header: 'Group Info',
            cssClass: 'my-custom-class',
            buttons: [{
                text: 'Edit Group Name',
                handler: () => {
                    this.editGroupDetails('Group Name', getGroupDetails?.displayName);
                }
            }, {
                text: 'Edit Group Status',
                handler: () => {
                    this.editGroupDetails('Group Status', getGroupDetails?.aboutStatus);
                }
            }, {
                text: 'Cancel',
                role: 'cancel',
                handler: () => { }
            }]
        });
        await actionSheet.present();
    }

    public async editGroupDetails(editType: any, editValue: any) {
        const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: editType,
            inputs: [
                {
                    name: 'edit',
                    type: 'text',
                    placeholder: editType,
                    value: editValue
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {}
                }, {
                    text: 'Ok',
                    handler: async (data) => {
                        if (editValue !== data.edit) {
                            const loading = await this.loadingController.create({
                                message: 'Loading...',
                                backdropDismiss: false,
                                keyboardClose: false,
                            });
                            loading.present();
                            const postData = {
                                groupID: this.chatUserDetails?.groupID,
                                url: '',
                                prevVal: editValue,
                                editedValue: data.edit,
                                isImage: false,
                                isGroupName: (editType === 'Group Name') ? true : false,
                                isGroupStatus: (editType === 'Group Status') ? true : false
                            };
    
                            this.chatService.updateGroupInfo(postData).then((res: any) => {
                                loading.dismiss();
                                if (res.msg && res?.result?.length > 0) {
                                    const getfindData = res?.result.find((ele: any) => ele.groupID === postData.groupID);
                                    const getChatUser = JSON.parse(localStorage.getItem('chatUser'));
                                    getChatUser.photoURL = null;
                                    getChatUser.displayName = '';
                                    getChatUser.aboutStatus = '';
                                    getChatUser.photoURL = getfindData?.photoURL;
                                    getChatUser.displayName = getfindData?.displayName;
                                    getChatUser.aboutStatus = getfindData?.aboutStatus;
                                    localStorage.setItem('chatUser', JSON.stringify(getChatUser));
                                }
                            }, async (er) => {
                                loading.dismiss();
                                const alert = await this.alertController.create({
                                    header: 'Update Failed',
                                    message: er.message,
                                    buttons: ['ok']
                                });
                                await alert.present();
                            });
                        }
                    }
                }
            ]
        });
        
        this.editGroupOkBtn = document.querySelector('ion-alert').querySelector('.alert-button-role-ok') as HTMLElement;
        if (this.editGroupOkBtn) {
            this.editGroupOkBtn.style.display = 'none';
        }

        await alert.present();
    }

    public async cameraClick() {
        const actionSheet = await this.actionSheetController.create({
            header: 'Photos',
            cssClass: 'my-custom-class',
            buttons: [{
                text: 'From Photos',
                handler: () => {
                    this.callGalleryCamera(CameraSource.Photos);
                }
            }, {
                text: 'Take Picture',
                handler: () => {
                    this.callGalleryCamera(CameraSource.Camera);
                }
            }, {
                text: 'Cancel',
                role: 'cancel',
                handler: () => { }
            }]
        });
        await actionSheet.present();
    }

    public async onScroll($event: any) {
        if ($event.target.localName != "ion-content") {
            return;
        }

        if ($event.detail.scrollTop > 205) {
            this.isHeaderVisible = false;
        }

        if ($event.detail.scrollTop === 0) {
            this.isHeaderVisible = true;
        }
    }

    public base64ToImage(dataURI: any) {
        const fileDate = dataURI.split(',');
        const byteString = atob(fileDate[1]);
        const arrayBuffer = new ArrayBuffer(byteString.length);
        const int8Array = new Uint8Array(arrayBuffer);
        for (let i = 0; i < byteString.length; i++) {
            int8Array[i] = byteString.charCodeAt(i);
        }
        const blob = new Blob([arrayBuffer], { type: 'image/png' });
        return blob;
    }

    public async callGalleryCamera(getCameraSource: any) {
        const loading = await this.loadingController.create({
            message: 'Loading...',
            backdropDismiss: false,
            keyboardClose: false,
        });

        const image = await Camera.getPhoto({
            quality: 100,
            allowEditing: false,
            resultType: CameraResultType.DataUrl,
            source: getCameraSource,
            saveToGallery: true
        }).then(async (getPhoto) => {
            await loading.present();
            const getGroupId = this.chatUserDetails?.groupID;
            const file: any = this.base64ToImage(getPhoto.dataUrl);
            const filePath = `groupProfileImages/${getGroupId}`;
            const customMetadata = { app: 'Group Profile Pictures' };
            // const fileRef = this.storage.ref(filePath);
            // this.storage.upload(filePath, file, { customMetadata }).snapshotChanges().pipe().subscribe((res) => {
            //     if (res?.state === 'success') {
            //         fileRef.getDownloadURL().subscribe(async (resp) => {
            //             if (resp.length > 0) {
            //                 const postData = {
            //                     groupID: this.chatUserDetails?.groupID,
            //                     url: resp,
            //                     isImage: true,
            //                     isGroupName: false,
            //                     isGroupStatus: false
            //                 };
            //                 this.chatService.updateGroupInfo(postData).then((res: any) => {
            //                     loading.dismiss();
            //                     if (res.msg && res?.result?.length > 0) {
            //                         const getUrl = res?.result.find((ele: any) => ele.groupID === postData.groupID);
            //                         const getChatUser = JSON.parse(localStorage.getItem('chatUser'));
            //                         getChatUser.photoURL = null;
            //                         getChatUser.photoURL = getUrl?.photoURL;
            //                         localStorage.setItem('chatUser', JSON.stringify(getChatUser));
            //                         this.chatUserDetails.photoURL = resp;
            //                     }
            //                 }, async (er) => {
            //                     loading.dismiss();
            //                     const alert = await this.alertController.create({
            //                         header: 'Upload Failed',
            //                         message: er.message,
            //                         buttons: ['ok']
            //                     });
            //                     await alert.present();
            //                 });
            //             }
            //         }, async (error) => {
            //             loading.dismiss();
            //             const alert = await this.alertController.create({
            //                 header: 'Upload Failed',
            //                 message: error.message,
            //                 buttons: ['ok']
            //             });
            //             await alert.present();
            //         });
            //     }
            // });
        }).catch(async (er) => {
            loading.dismiss();
            const alert = await this.alertController.create({
                header: 'Cancelled',
                message: er.message,
                buttons: ['ok']
            });
            await alert.present();
        });
    }

    public searchClear(event: any) {
        this.usersList = this.dupUsersList;
        this.alert.inputs = [];
        this.alert.inputs = this.dupUsersList;
    }

    public searchUser(event: any) {
        const getVal = event?.target?.value;
        this.usersList = this.dupUsersList;
        if (getVal.trim() == '') {
            this.alert.inputs = [];
            this.alert.inputs = this.usersList;
            return;
        }
        this.usersList = this.usersList.filter((ele: any) => {
            if ((ele.displayName.toLowerCase().indexOf(getVal.toLowerCase())) > -1) {
                return true;
            }
            return false;
        });
        this.alert.inputs = this.usersList;
    }

    public filterGroupMembers() {
        this.chatService.getAllUserMethod().subscribe((resp) => {
            this.usersList = [];
            this.dupUsersList = [];
            let getFilteredUsersList = [];

            if (resp?.length > 0) {
                if (this.chatUserDetails?.groupMembers?.length > 0) {
                    getFilteredUsersList = resp.filter(({ uid: id1 }) => !this.chatUserDetails?.groupMembers.some(({ uid: id2 }) => id2 === id1));
                    if (getFilteredUsersList?.length > 0) {
                        const getResults = this.groupMembersCallBack(getFilteredUsersList);
                        this.usersList = getResults[0];
                        this.dupUsersList = getResults[1];
                    } else {
                        const getResults = this.groupMembersCallBack(resp);
                        this.usersList = getResults[0];
                        this.dupUsersList = getResults[1];
                    }
                } else {
                    const getResults = this.groupMembersCallBack(resp);
                    this.usersList = getResults[0];
                    this.dupUsersList = getResults[1];
                }
            }
        });
    }

    public groupMembersCallBack(usersListArray: any) {
        let newUsers = usersListArray;
        let dupNewUsers = usersListArray;

        newUsers.map((res: any) => {
            res.name = res.uid,
                res.type = 'checkbox',
                res.label = res.displayName,
                res.value = res.uid,
                res.checked = false
            res.handler = () => {
                res.checked = !res.checked;
                this.dupUsersList.find((ele: any) => {
                    if (ele.uid === res.uid) {
                        ele.checked = res.checked;
                    }
                });
            }
        });

        dupNewUsers.map((res: any) => {
            res.name = res.uid,
                res.type = 'checkbox',
                res.label = res.displayName,
                res.value = res.uid,
                res.checked = false
            res.handler = () => {
                res.checked = !res.checked;
                const getSelected = dupNewUsers.filter((ele: any) => ele.checked === true);
                if (getSelected?.length > 0) {
                    if (this.getalertOkBtn) {
                        this.getalertOkBtn.style.display = 'block';
                    }
                } else {
                    if (this.getalertOkBtn) {
                        this.getalertOkBtn.style.display = 'none';
                    }
                }
            }
        });

        return [newUsers, dupNewUsers];
    }

    public async getMembersListPopupClick() {
        this.filterGroupMembers();
        this.alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Select Members',
            message: '<ion-searchbar></ion-searchbar>',
            inputs: this.usersList,
            animated: true,
            backdropDismiss: false,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => { }
                }, {
                    text: 'Ok',
                    role: 'ok',
                    handler: async () => {
                        const loading = await this.loadingController.create({
                            message: 'Adding...',
                            backdropDismiss: false,
                            keyboardClose: false
                        });
                        loading.present();

                        let getSelectedMembers = [];
                        this.selecteGroupMembers = [];
                        this.selecteGroupMembers = this.usersList.filter((ele: any) => ele.checked === true);
                        let date: number = Date.now();

                        getSelectedMembers = this.selecteGroupMembers.map((res: any, index: any) => {
                            const getIndex = Number(Number(index) + Number(1));
                            const resp = {
                                aboutStatus: res.aboutStatus,
                                displayName: res.displayName,
                                email: res.email,
                                photoURL: res.photoURL ? res.photoURL : 'null',
                                uid: res.uid,
                                createdAt: Number(Number(date) + Number(getIndex)),
                                isAdmin: false
                            };
                            return resp;
                        });

                        const postData = {
                            displayName: this.chatUserDetails?.displayName,
                            aboutStatus: this.chatUserDetails?.aboutStatus,
                            photoURL: this.chatUserDetails?.photoURL ? this.chatUserDetails?.photoURL : null,
                            isGroup: true,
                            createdAt: this.chatUserDetails?.createdAt,
                            createdBy: this.chatUserDetails?.createdBy,
                            createdName: this.chatUserDetails?.createdName
                        };

                        const combinedPostData = {
                            postData: postData,
                            groupMembers: getSelectedMembers,
                            groupID: this.chatUserDetails?.groupID,
                            chatID: this.chatUserDetails?.chatId
                        };

                        this.chatService.updateGroupMembers(combinedPostData).then((res: any) => {
                            loading.dismiss();
                            if (res?.msg && res?.result?.length > 0) {
                                const getChatUser = res.result.find((ele: any) => ele.groupID === this.chatUserDetails?.groupID);
                                const getCurrenChatUser = JSON.parse(localStorage.getItem('chatUser'));
                                localStorage.removeItem('chatUser')
                                getCurrenChatUser.groupMembers = [];
                                getCurrenChatUser.groupMembers = getChatUser?.groupMembers;
                                this.getMembersList = getChatUser?.groupMembers;
                                this.getDupMembersList = getChatUser?.groupMembers;
                                localStorage.setItem('chatUser', JSON.stringify(getCurrenChatUser));
                            }
                        }, (error: any) => {
                            console.error(error);
                        });
                    }
                }
            ]
        });

        if (this.usersList?.length > 0) {
            let ele = document.querySelector('ion-searchbar');
            if (ele) {
                ele.addEventListener('input', this.searchUser.bind(this));
                ele.addEventListener('ionClear', this.searchClear.bind(this));
            }
        }

        this.getalertOkBtn = document.querySelector('ion-alert').querySelector('.alert-button-role-ok') as HTMLElement;
        if (this.getalertOkBtn) {
            this.getalertOkBtn.style.display = 'none';
        }
        await this.alert.present();
    }

    public searchMembersClick() {
        this.isShowMemberSearch = !this.isShowMemberSearch;
    }

    public searchMemberClear(event: any) {
        this.searchMembersText = '';
        this.getMembersList = this.getDupMembersList;
    }

    public searchMembers(value: any) {
        const getVal = value;
        this.getMembersList = this.getDupMembersList;
        if (getVal.trim() == '') {
            return;
        }
        this.getMembersList = this.getMembersList.filter((ele: any) => {
            if ((ele.displayName.toLowerCase().indexOf(getVal.toLowerCase())) > -1) {
                return true;
            }
            return false;
        });
    }

    public async showProfileCardPopOver(user: any) {
        if (user.uid !== this.getCurrentUser.uid) {
            const popover = await this.popoverCtrl.create({
                component: ProfileCardPopoverComponent,
                componentProps: {
                    chatUser: user
                },
                cssClass: 'profile-card-class'
            });
            await popover.present();
        }
    }

    public commonGroupChatClick(getGroup: any) {
        localStorage.setItem('chatUser', JSON.stringify(getGroup));
        this.router.navigateByUrl('/chats', { replaceUrl: true });
    }

    public async chatClick(getUser: any) {
        const currentUserInfo = JSON.parse(localStorage.getItem('currentUser'));
        const getChatGroupData = JSON.parse(localStorage.getItem('chatUser'));
        if (getUser.uid !== this.getCurrentUser.uid) {
            if (getChatGroupData.groupMembers.length > 0) {
                const getAdminData = getChatGroupData.groupMembers.find((ele: any) => ele.uid === currentUserInfo.uid);
                if (getAdminData) {
                    const popover = await this.popoverCtrl.create({
                        component: AccessPopoverComponent,
                        componentProps: {
                            chatUser: getUser,
                            isAdmin: getAdminData.isAdmin
                        },
                        cssClass: 'access-popover-class'
                    });
                    await popover.present();
                }
            }
        }
    }

    public async exitGroupClick() {
        const getChatGroupData = JSON.parse(localStorage.getItem('chatUser'));
        const currentUserInfo = JSON.parse(localStorage.getItem('currentUser'));
        const alert = await this.alertController.create({
            message: `Exit ${this.chatUserDetails?.displayName} group?`,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => { }
                }, {
                    text: 'Exit',
                    handler: async () => {
                        const loading = await this.loadingController.create({
                            message: 'Removing...',
                            backdropDismiss: false,
                            keyboardClose: false,
                        });
                        await loading.present();
                        const getChatGroupData1 = JSON.parse(localStorage.getItem('chatUser'));
                        const checkIsAdmin = getChatGroupData1.groupMembers.find((ele: any) => ele.uid === currentUserInfo.uid)?.isAdmin;
                        delete getChatGroupData.groupMembers
                        const groupData = getChatGroupData;
                        delete currentUserInfo.friendsList;
                        const userData = currentUserInfo;
                        const postData = {
                            chatGroupData: groupData,
                            currentUser: userData,
                            removedBy: userData,
                            isAdmin: false,
                            newAdmin: null
                        };
                        if (checkIsAdmin) {
                            const checkNextAdmin = getChatGroupData1.groupMembers.filter((ele: any) => (ele.uid !== currentUserInfo.uid && ele.isAdmin === true));
                            if (checkNextAdmin.length <= 0) {
                                const makeNewAdmin = getChatGroupData1.groupMembers.filter((fele: any) => fele.isAdmin === false).sort((a: any, b: any) => a.createdAt - b.createdAt);
                                makeNewAdmin[0].isAdmin = true;
                                postData.isAdmin = true;
                                postData.newAdmin = makeNewAdmin[0];
                            } else {
                                postData.isAdmin = false;
                                postData.newAdmin = null;
                            }
                        } else {
                            postData.isAdmin = false;
                            postData.newAdmin = null;
                        }

                        this.chatService.exitFromGroup(postData).then((res: any) => {
                            loading.dismiss();
                            if (res?.msg && res?.result?.length > 0) {
                                const getChatUser = res.result.find((ele: any) => ele.groupID === this.chatUserDetails?.groupID);
                                const getCurrenChatUser = JSON.parse(localStorage.getItem('chatUser'));
                                getCurrenChatUser.groupMembers = [];
                                getCurrenChatUser.groupMembers = getChatUser?.groupMembers;
                                this.getMembersList = getChatUser?.groupMembers;
                                this.getDupMembersList = getChatUser?.groupMembers;
                                localStorage.setItem('chatUser', JSON.stringify(getCurrenChatUser));
                            }
                        }).catch((error) => {
                            console.error(error);
                        });
                    }
                }
            ]
        });
        await alert.present();
    }

    ngOnDestroy() {
        if (this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();
        }
    }
}
