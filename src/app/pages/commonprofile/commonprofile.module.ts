import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CommonProfilePageRoutingModule } from './commonprofile-routing.module';
import { CommonProfilePage } from './commonprofile.page';
import { ScrollbarThemeModule } from 'src/app/services/scrollbarDirective';
import { AccessPopoverComponent } from 'src/app/shared/access-popover';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CommonProfilePageRoutingModule,
        ScrollbarThemeModule
    ],
    declarations: [
        CommonProfilePage,
        AccessPopoverComponent
    ]
})

export class CommonProfilePageModule { }
