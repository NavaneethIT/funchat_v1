import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommonProfilePage } from './commonprofile.page';

const routes: Routes = [
    {
        path: '',
        component: CommonProfilePage
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class CommonProfilePageRoutingModule { }
