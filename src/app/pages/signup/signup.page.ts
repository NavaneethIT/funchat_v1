import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { ChatService } from 'src/app/services/chat.service';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.page.html',
    styleUrls: ['./signup.page.scss'],
    standalone: false
})

export class SignupPage implements OnInit {

    public credentialForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private chatService: ChatService,
        private alertController: AlertController,
        private loadingController: LoadingController
    ) { }

    ngOnInit() {
        this.credentialForm = this.fb.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
            userName: ['',  [Validators.required, Validators.minLength(5)]]
        });
    }

    public async signUpClick() {
        const loading = await this.loadingController.create({
            message: 'Sign Up...',
        });
        await loading.present();

        this.chatService.signUp(this.credentialForm.value).then((res: any) => {
            loading.dismiss();
            if (res.msg) {
                // this.chatService.createPeerConnection(res.result.uid);
                localStorage.setItem('currentUser', JSON.stringify(res.result));
                this.router.navigateByUrl('tabs', { replaceUrl: true });
            }
        }, async (error: any) => {
            loading.dismiss();
            const alert = await this.alertController.create({
                header: 'Sign Up Failed',
                message: error.message,
                buttons: ['ok']
            });
            await alert.present();
        });
    }

    public signInClick() {
        this.router.navigateByUrl('signin', { replaceUrl: true });
    }

    get userName() {
        return this.credentialForm.get('userName');
    }

    get email() {
        return this.credentialForm.get('email');
    }

    get password() {
        return this.credentialForm.get('password');
    }

}
