import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TabsPageRoutingModule } from './tabs-routing.module';
import { TabsPage } from './tabs.page';
import { ProfilePopoverComponent } from '../../shared/profile-popover';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        TabsPageRoutingModule
    ],
    declarations: [
        TabsPage,
        ProfilePopoverComponent
    ]
})

export class TabsPageModule { }
