import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { ChatService } from 'src/app/services/chat.service';
import { ProfilePopoverComponent } from '../../shared/profile-popover';
// import circular from 'circular-json';
// import Peer from 'peerjs';

@Component({
    selector: 'app-tabs',
    templateUrl: './tabs.page.html',
    styleUrls: ['./tabs.page.scss'],
    standalone: false
})

export class TabsPage implements OnInit {

    public selectedTab: string = 'chatlist';
    public getCurrentUser: any;
    public profileImage: any;
    public isShowSearch: boolean = false;
    public searchText: string = '';

    constructor(
        private popoverCtrl: PopoverController,
        private router: Router,
        private chatService: ChatService
    ) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
        // this.chatService._peer = circular.parse(localStorage.getItem('peerConnection')) as Peer;
    }

    ngOnInit() {
    }

    public tabClicked(event: any) {
        this.selectedTab = event.tab;
        this.router.navigateByUrl(`tabs/${this.selectedTab}`, { replaceUrl: true });
    }

    public searchClick() {
        this.isShowSearch = !this.isShowSearch;
    }

    public searchCancelClick(value: any) {
        this.isShowSearch = false;
        this.searchText = '';
        this.chatService.setSearchText(this.searchText);
    }

    public searchClearClick(event: any) {
        this.searchText = '';
        this.chatService.setSearchText(this.searchText);
    }

    public searchChatUserList(event: any) {
        this.searchText = event;
        this.chatService.setSearchText(event);
    }

    public async showPopover(event: any) {
        const popover = await this.popoverCtrl.create({
            component: ProfilePopoverComponent,
            event
        });
        await popover.present();
    }

    public newUserListClick() {
        this.router.navigateByUrl('userlist', { replaceUrl: true });
    }
}
