import { Injectable } from '@angular/core';
import Peer from 'peerjs';

import {WebRTCConfig} from './webrtc.config';


@Injectable()
export class WebRTCService {

    private _peer: Peer;
    private _localStream: any;
    private _existingCall: any;
    
    myEl: HTMLMediaElement;
    otherEl: HTMLMediaElement;
    onCalling: Function;

    constructor(private config: WebRTCConfig) {
        navigator.getUserMedia = navigator.getUserMedia;
    }

    
}
