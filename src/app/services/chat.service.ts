import { inject, Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, from } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { FriendsListModel } from '../models/chat-model';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore } from '@angular/fire/compat/firestore';
// import Peer from 'peerjs';
// import { WebRTCConfig } from './webrtc.config';
// import circular from 'circular-json';
import { Firestore, doc, updateDoc, arrayRemove, arrayUnion, collection, query, where, documentId, onSnapshot, setDoc, getDocs } from '@angular/fire/firestore';
import { Auth, createUserWithEmailAndPassword, getAuth, updateProfile } from '@angular/fire/auth';
import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';

@Injectable()

export class ChatService {

    public getCurrentUser: any;
    private searchText = new BehaviorSubject<any>('');
    private allUserList = new BehaviorSubject<any>([]);
    private isSearch = new BehaviorSubject<any>(false);
    private setGroupListData = new BehaviorSubject<any>([]);
    public auth = getAuth();

    // public _peer: Peer;
    public _localStream: any;
    public _existingCall: any;
    public myEl: HTMLMediaElement;
    public otherEl: HTMLMediaElement;
    public onCalling: Function;
    public newCall: boolean = false;

    constructor(
        public afAuth: AngularFireAuth,
        public afs: AngularFirestore,
        private firestore: Firestore
    ) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.setAllUserMethod();
        this.auth = getAuth();
    }

    public signin(credentials: any) {
        const signInPromise = new Promise((res, rej) => {
            this.afAuth.signInWithEmailAndPassword(credentials.email, credentials.password).then((getRes) => {
                this.getSingleUser(getRes.user.uid).subscribe((userResp: any) => {
                    res({
                        msg: true,
                        result: userResp
                    });
                }, (error: any) => {
                    rej(error);
                });
            }).catch((err: any) => {
                rej(err);
            });
        });
        return signInPromise;
    }

    public signUp(credentials: any) {
        const signUpPromise = new Promise((res, rej) => {
            this.afAuth.createUserWithEmailAndPassword(credentials.email, credentials.password).then((getRes: any) => {
                getRes.user.updateProfile({
                    displayName: credentials.userName,
                    photoURL: null
                });
                const insertUserDoc = doc(this.firestore, `users/${getRes.user.uid}`);
                setDoc(insertUserDoc, {
                    uid: getRes.user.uid,
                    email: credentials.email,
                    displayName: credentials.userName,
                    aboutStatus: 'Hey there! I am using FunChat',
                    photoURL: null,
                    friendsList: [],
                    isGroup: false,
                    createdAt: Date.now()
                }, { merge: true }).then((resp) => {
                    this.getSingleUser(getRes?.user?.uid).subscribe((userResp: any) => {
                        res({
                            msg: true,
                            result: userResp
                        });
                    }, (error: any) => {
                        rej(error);
                    });
                }).catch((err: any) => {
                    rej(err);
                });
            }).catch((err: any) => {
                rej(err);
            });
        });
        return signUpPromise;
    }

    public signOut() {
        return this.afAuth.signOut();
    }

    public forgetPassword(postData: any) {
        return new Promise((res, rej) => {
            this.afAuth.sendPasswordResetEmail(postData.email).then((resp: any) => {
                res(true);
            }).catch((error: any) => {
                rej(error);
            });
        });
    }

    public getSingleUser(userId: any) {
        const singleUserDoc = doc(this.firestore, `users/${userId}`);
        return new Observable(observer => {
            const unsubscribe = onSnapshot(singleUserDoc, (docSnapshot) => {
              if (docSnapshot.exists()) {
                observer.next(docSnapshot.data());
              }
            });
            return () => unsubscribe();
        });
    }

    public getAllUser()  {
        const allUserDocs = collection(this.firestore, 'users');
        return from(getDocs(allUserDocs)).pipe(
            map((querySnapshot) => {
                return querySnapshot.docs.map(doc => doc.data());
            })
        );
    }

    public updateProfilePic(postData: any) {
        return new Promise((res, rej) => {
            this.afAuth.currentUser.then((userData: any) => {
                userData.updateProfile({
                    photoURL: postData.url
                }).then((gtRes: any) => {
                    this.afs.collection('users').doc(postData.uid).update({ photoURL: postData.url }).then((resp: any) => {
                        if (postData.friendsList.length > 0) {
                            postData.friendsList.forEach((frdEle: any, index: any, data: any) => {
                                this.afs.collection('users').doc(frdEle.uid).get().subscribe((frdRes: any) => {
                                    let currentUser = frdRes.data()?.friendsList.find((ele: any) => ele.uid === this.getCurrentUser?.uid);
                                    this.afs.collection('users').doc(frdEle.uid).ref.update({
                                        friendsList: firebase.firestore.FieldValue.arrayRemove(currentUser)
                                    }).then((remRes: any) => {
                                        currentUser.photoURL = postData.url;
                                        this.afs.collection('users').doc(frdEle.uid).ref.update({
                                            friendsList: firebase.firestore.FieldValue.arrayUnion(currentUser)
                                        }).then((updatedRes: any) => {
                                            res(true);
                                        }).catch((error: any) => {
                                            rej(error);
                                        });
                                    }).catch((error: any) => {
                                        rej(error);
                                    });
                                });
                            });
                        } else {
                            res(true);
                        }
                    }).catch((err: any) => {
                        rej(err);
                    });
                })
            });
        });
    }

    public updateFriendsList(getCurrentUser: any, getFrdData: any, getChatId: any) {
        const curentUserFriendPostData = {
            uid: getFrdData.uid,
            chatId: getChatId,
            aboutStatus: getFrdData.aboutStatus,
            displayName: getFrdData.displayName,
            email: getFrdData.email,
            photoURL: getFrdData.photoURL,
            createdAt: getFrdData.createdAt,
            isGroup: false
        };

        const chatUserFriendPostData = {
            uid: getCurrentUser.uid,
            chatId: getChatId,
            aboutStatus: getCurrentUser.aboutStatus,
            displayName: getCurrentUser.displayName,
            email: getCurrentUser.email,
            photoURL: getCurrentUser.photoURL,
            createdAt: getCurrentUser.createdAt,
            isGroup: false
        };

        return new Promise((res, rej) => {
            const userDocRef = doc(this.firestore, `users/${getCurrentUser.uid}`);
            updateDoc(userDocRef, {
                friendsList: arrayUnion(curentUserFriendPostData)
            }).then((resp) => {
                const frdDocRef = doc(this.firestore, `users/${getFrdData.uid}`);
                updateDoc(frdDocRef, {
                    friendsList: arrayUnion(chatUserFriendPostData) 
                }).then((respo) => {
                    res(respo);
                }).catch((err) => {
                    rej(err);
                })
            }).catch((error) => {
                rej(error);
            });
        });
    }

    public addNewMessage(getMsg: any, frdData: any, isImage: boolean, getChatId: any) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
        const postData = {
            uid: this.getCurrentUser?.uid,
            userName: this.getCurrentUser?.displayName,
            content: getMsg,
            dummyMsg: '',
            receiverUid: frdData?.uid,
            receiverName: frdData?.displayName,
            createdAt: Date.now(),
            isFirst: false,
            isGroup: false,
            isDummy: false,
            isAdminMsg: false,
            isImage: isImage
        };

        if (this.getCurrentUser?.friendsList?.length > 0) {
            const getChatUser = this.getCurrentUser?.friendsList?.find((ele: FriendsListModel[0]) => ele.uid === frdData.uid);
            if (getChatUser && getChatUser?.chatId) {
                getChatId = getChatUser?.chatId;
                const messageDocRef = doc(this.firestore, `messages/${getChatId}`);
                return updateDoc(messageDocRef, {
                    messages: arrayUnion(postData)
                });
            } else {
                const newPostData = {
                    chatId: getChatId,
                    messages: [{
                        uid: this.getCurrentUser?.uid,
                        userName: this.getCurrentUser?.displayName,
                        content: getMsg,
                        dummyMsg: '',
                        receiverUid: frdData?.uid,
                        receiverName: frdData?.displayName,
                        createdAt: Date.now(),
                        isFirst: false,
                        isGroup: false,
                        isDummy: false,
                        isAdminMsg: false,
                        isImage: isImage
                    }]
                };
                return new Promise<any>((res, rej) => {
                    const msgDocRef = doc(this.firestore, `messages/${getChatId}`);
                    setDoc(msgDocRef, newPostData).then((resp) => {
                        localStorage.setItem('chatID', getChatId);
                        this.updateFriendsList(this.getCurrentUser, frdData, getChatId).then((upRes: any) => {
                            this.mergeFriendAndGroupList().subscribe((getUserResp: any) => {
                                res({
                                    msg: true,
                                    result: getUserResp
                                });
                            }, (error: any) => {
                                rej(error);
                            });
                        }).catch((error: any) => {
                            rej(error);
                        });
                    });
                });
            }
        } else {
            const newPostData = {
                chatId: getChatId,
                messages: [{
                    uid: this.getCurrentUser?.uid,
                    userName: this.getCurrentUser?.displayName,
                    content: getMsg,
                    dummyMsg: '',
                    receiverUid: frdData?.uid,
                    receiverName: frdData?.displayName,
                    createdAt: Date.now(),
                    isFirst: false,
                    isGroup: false,
                    isDummy: false,
                    isAdminMsg: false,
                    isImage: isImage
                }]
            };

            return new Promise<any>((res, rej) => {
                const msgDocRef = doc(this.firestore, `messages/${getChatId}`);
                setDoc(msgDocRef, newPostData).then((resp) => {
                    localStorage.setItem('chatID', getChatId);
                    this.updateFriendsList(this.getCurrentUser, frdData, getChatId).then((upRes: any) => {
                        this.mergeFriendAndGroupList().subscribe((getUserResp: any) => {
                            res({
                                msg: true,
                                result: getUserResp
                            });
                        }, (error: any) => {
                            rej(error);
                        });
                    }).catch((error: any) => {
                        rej(error);
                    });
                });
            });
        }
    }

    public createGroup(postData: any, groupMembers: any, getGroupID: any) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
        const groupID = (getGroupID !== '' && getGroupID !== null && getGroupID !== undefined && getGroupID !== 'null') ? getGroupID : this.afs.createId();
        const getChatId = this.afs.createId();
        const db = firebase.firestore();
        const groupBatch = db.batch();
        const newBatch = db.batch();
        let length = 0;

        return new Promise(async (res, rej) => {
            if (groupMembers.length > 0) {
                groupMembers.forEach((ele: any, index: any, data: any) => {
                    firebase.firestore().collection('groups').doc(ele.uid).get().then((exsRes: any) => {
                        if (!exsRes?.exists) {
                            const docRef = db.collection('groups').doc(ele.uid);
                            postData.groupID = groupID;
                            postData.chatId = getChatId;
                            groupBatch.set(docRef, { groups: [postData] });
                        } else {
                            const updateDocRef = db.collection('groups').doc(ele.uid);
                            postData.groupID = groupID;
                            postData.chatId = getChatId;
                            groupBatch.update(updateDocRef, { groups: firebase.firestore.FieldValue.arrayUnion(postData) });
                        }
                        length++;

                        if (length === data.length) {
                            callBatch(this);
                        }
                    }).catch(error => {
                        rej(error);
                    });
                });

                function callBatch(copyThis: any) {
                    groupBatch.commit().then((resp: any) => {
                        copyThis.afs.collection('groupMembers').doc(groupID).set({
                            groupID: groupID,
                            photoURL: postData.photoURL,
                            groupMembers: groupMembers,
                            createdBy: copyThis.getCurrentUser?.uid,
                            aboutStatus: postData.aboutStatus,
                            displayName: postData.displayName
                        }).then((respo: any) => {
                            let newPostData = {
                                chatId: getChatId,
                                messages: [{
                                    uid: copyThis.getCurrentUser?.uid,
                                    userName: copyThis.getCurrentUser?.displayName,
                                    content: '',
                                    dummyMsg: 'Created this Group',
                                    receiverUid: copyThis.getCurrentUser?.uid,
                                    receiverName: copyThis.getCurrentUser?.displayName,
                                    createdAt: Date.now(),
                                    isFirst: true,
                                    isGroup: true,
                                    isDummy: true,
                                    isAdminMsg: false,
                                    isImage: false
                                }]
                            };
                            copyThis.afs.collection('messages').doc(getChatId).set(newPostData).then((msgRes: any) => {
                                groupMembers.forEach((ele: any) => {
                                    if (ele.uid !== copyThis.getCurrentUser?.uid) {
                                        const docRef = db.collection('messages').doc(getChatId);
                                        const msgPostData = {
                                            uid: copyThis.getCurrentUser?.uid,
                                            userName: copyThis.getCurrentUser?.displayName,
                                            content: '',
                                            dummyMsg: 'Added',
                                            receiverUid: ele.uid,
                                            receiverName: ele.displayName,
                                            createdAt: Date.now(),
                                            isFirst: false,
                                            isGroup: true,
                                            isDummy: true,
                                            isAdminMsg: false,
                                            isImage: false
                                        };
                                        newBatch.update(docRef, { messages: firebase.firestore.FieldValue.arrayUnion(msgPostData) });
                                    }
                                });
                                newBatch.commit().then((updateRes: any) => {
                                    res(true);
                                }).catch((error: any) => {
                                    rej(error);
                                });
                            }).catch((error: any) => {
                                rej(error);
                            });
                        }).catch((error: any) => {
                            rej(error);
                        });
                    }).catch((error: any) => {
                        rej(error);
                    });
                }
            }
        });
    }

    public getGroups() {
        return this.afs.collection('groups').doc(this.auth.currentUser.uid).snapshotChanges().pipe(map((groupEle: any) => {
            return groupEle.payload.data();
        }));
    }

    public getGroupMembersList() {
        let getGroupList = [];
        return this.getGroups().pipe(
            switchMap((groups: any) => {
                if (groups) {
                    let getGroupIDList: any = [];
                    groups?.groups.forEach((element: any) => {
                        getGroupIDList.push(element.groupID);
                    });
                    getGroupList = groups?.groups;
                    if (getGroupList?.length > 0) {

                        const groupMembersCollection = collection(this.firestore, 'groupMembers');

                        // Build the query
                        const q = query(
                            groupMembersCollection,
                            where(documentId(), 'in', getGroupIDList)
                        );

                        return getDocs(q);
                        
                        // return this.afs.collection('groupMembers').ref.where(documentId(), "in", getGroupIDList).get();
                    } else {
                        return [];
                    }
                } else {
                    getGroupList = [];
                    return of(getGroupList);
                }
            }),
            map((groupMembers: any) => {
                if (groupMembers?.docs?.length > 0) {
                    getGroupList.map((groupEle: any) => {
                        const getSingleGroup = groupMembers?.docs.find((ele: any) => ele?.data()?.groupID === groupEle.groupID);
                        groupEle.groupMembers = getSingleGroup?.data()?.groupMembers;
                        groupEle.photoURL = getSingleGroup?.data()?.photoURL;
                        groupEle.displayName = getSingleGroup?.data()?.displayName;
                        groupEle.aboutStatus = getSingleGroup?.data()?.aboutStatus;
                    });
                } else {
                    getGroupList = [];
                }
                return getGroupList;
            })
        );
    }

    public getSingleGroupMembersList(groupId: any) {
        return this.afs.collection('groupMembers').doc(groupId).snapshotChanges();
    }

    public mergeFriendAndGroupList(): Observable<any> {
        let getChatFrienGroupList: any = [];
        let userData: any;
        return this.getSingleUser(this.auth.currentUser.uid).pipe(
            switchMap((userRes: any) => {
                userData = userRes;
                const getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
                getCurrentUser.friendsList = [];
                getCurrentUser.friendsList = userRes.friendsList;
                localStorage.setItem('currentUser', JSON.stringify(getCurrentUser));
                return this.getGroupMembersList().pipe(map((res: any) => {
                    return res;
                }));
            }),
            map((groupRes: any) => {
                getChatFrienGroupList = [];
                const getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
                if (groupRes?.length > 0) {
                    this.setGroupList(groupRes);
                    groupRes.forEach((groupEle: any) => {
                        groupEle.lastMessage = '';
                        groupEle.lastMessageAt = 0;
                        groupEle.isImage = false;
                        groupEle.senderId = '';
                        groupEle.senderName = '';
                        groupEle.isDummy = false;
                        this.getMessages(groupEle.chatId).subscribe((resp: any) => {
                            if (resp.length > 0) {
                                resp.forEach((msgEle: any) => {
                                    if (msgEle.isDummy && !msgEle.isAdminMsg) {
                                        groupEle.lastMessage = (msgEle?.isDummy) ?
                                            msgEle?.dummyMsg : msgEle?.content;
                                        groupEle.lastMessageAt = msgEle?.createdAt;
                                        groupEle.isImage = msgEle?.isImage;
                                        groupEle.senderId = msgEle?.uid;
                                        groupEle.senderName = msgEle?.userName;
                                        groupEle.isDummy = msgEle?.isDummy;
                                    } else if (msgEle.isDummy && msgEle.isAdminMsg && msgEle.receiverUid === getCurrentUser.uid) {
                                        groupEle.lastMessage = (msgEle?.isDummy) ?
                                            msgEle?.dummyMsg : msgEle?.content;
                                        groupEle.lastMessageAt = msgEle?.createdAt;
                                        groupEle.isImage = msgEle?.isImage;
                                        groupEle.senderId = msgEle?.uid;
                                        groupEle.senderName = msgEle?.userName;
                                        groupEle.isDummy = msgEle?.isDummy;
                                    } else {
                                        groupEle.lastMessage = (msgEle?.isDummy) ?
                                            msgEle?.dummyMsg : msgEle?.content;
                                        groupEle.lastMessageAt = msgEle?.createdAt;
                                        groupEle.isImage = msgEle?.isImage;
                                        groupEle.senderId = msgEle?.uid;
                                        groupEle.senderName = msgEle?.userName;
                                        groupEle.isDummy = msgEle?.isDummy;
                                    }
                                });
                            }
                        });
                        getChatFrienGroupList.push(groupEle);
                    });
                } else {
                    getChatFrienGroupList = [];
                }

                if (userData) {
                    userData?.friendsList.forEach((userEle: any) => {
                        userEle.lastMessage = '';
                        userEle.lastMessageAt = 0;
                        userEle.isImage = false;
                        userEle.senderId = '';
                        userEle.senderName = '';
                        userEle.isDummy = false;
                        this.getMessages(userEle.chatId).subscribe((resp: any) => {
                            if (resp.length > 0) {
                                const lastData = resp[resp.length - 1];
                                userEle.lastMessage = (lastData?.isDummy) ?
                                    lastData?.dummyMsg : lastData?.content;
                                userEle.lastMessageAt = lastData?.createdAt;
                                userEle.isImage = lastData?.isImage;
                                userEle.senderId = lastData?.uid;
                                userEle.senderName = lastData?.userName;
                                userEle.isDummy = lastData?.isDummy;
                            }
                        });
                        getChatFrienGroupList.push(userEle);
                    });
                } else {
                    getChatFrienGroupList = [];
                }
                return getChatFrienGroupList;
            })
        );
    }

    public updateGroupInfo(postData: any) {
        return new Promise((res, rej) => {
            const msgPostData = {
                uid: this.getCurrentUser?.uid,
                userName: this.getCurrentUser?.displayName,
                content: '',
                dummyMsg: '',
                receiverUid: '',
                receiverName: '',
                createdAt: Date.now(),
                isFirst: false,
                isGroup: true,
                isDummy: true,
                isAdminMsg: false,
                isImage: false
            };
            let postDetails: any;
            let newMsg = '';
            if (postData.isImage) {
                postDetails = { photoURL: postData.url };
                msgPostData.dummyMsg = `Changed this group's icon`;
            } else if (postData.isGroupName) {
                postDetails = { displayName: postData.editedValue };
                msgPostData.dummyMsg = `Changed the Group Name from "${postData.prevVal}" to "${postData.editedValue}"`;
            } else {
                postDetails = { aboutStatus: postData.editedValue };
                msgPostData.dummyMsg = `Changed the Group Status from "${postData.prevVal}" to "${postData.editedValue}"`;
            }

            this.afs.collection('groupMembers').doc(postData.groupID).update(postDetails).then((resp: any) => {
                this.addNewGroupMessage(msgPostData).then((msgRes: any) => {
                    this.mergeFriendAndGroupList().subscribe((getUserResp: any) => {
                        res({
                            msg: true,
                            result: getUserResp
                        });
                    }, (error: any) => {
                        rej(error);
                    });
                }).catch((error: any) => {
                    rej(error);
                });
            }).catch((error: any) => {
                rej(error);
            });
        });
    }

    public updateGroupMembers(combinedPostData: any) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
        const postData = combinedPostData.postData;
        const groupMembers = combinedPostData.groupMembers;
        const groupID = combinedPostData.groupID;
        const getChatId = combinedPostData.chatID;
        const db = firebase.firestore();
        const groupBatch = db.batch();
        const newBatch = db.batch();
        let length = 0;

        return new Promise(async (res, rej) => {
            if (groupMembers.length > 0) {
                groupMembers.forEach((ele: any, index: any, data: any) => {
                    firebase.firestore().collection('groups').doc(ele.uid).get().then((exsRes: any) => {
                        if (!exsRes?.exists) {
                            const docRef = db.collection('groups').doc(ele.uid);
                            postData.groupID = groupID;
                            postData.chatId = getChatId;
                            groupBatch.set(docRef, { groups: [postData] });
                        } else {
                            const updateDocRef = db.collection('groups').doc(ele.uid);
                            postData.groupID = groupID;
                            postData.chatId = getChatId;
                            groupBatch.update(updateDocRef, { groups: firebase.firestore.FieldValue.arrayUnion(postData) });
                        }
                        length++;

                        if (length === data.length) {
                            callBatch(this);
                        }
                    }).catch(error => {
                        rej(error);
                    });
                });

                function callBatch(copyThis: any) {
                    groupBatch.commit().then((resp: any) => {
                        groupMembers.forEach((ele: any) => {
                            if (ele.uid !== copyThis.getCurrentUser?.uid) {
                                const gmRef = db.collection('groupMembers').doc(groupID);
                                const msgRef = db.collection('messages').doc(getChatId);
                                const msgPostData = {
                                    uid: copyThis.getCurrentUser?.uid,
                                    userName: copyThis.getCurrentUser?.displayName,
                                    content: '',
                                    dummyMsg: 'Added',
                                    receiverUid: ele.uid,
                                    receiverName: ele.displayName,
                                    createdAt: Date.now(),
                                    isFirst: false,
                                    isGroup: true,
                                    isDummy: true,
                                    isAdminMsg: false,
                                    isImage: false
                                };
                                newBatch.update(gmRef, { groupMembers: firebase.firestore.FieldValue.arrayUnion(ele) });
                                newBatch.update(msgRef, { messages: firebase.firestore.FieldValue.arrayUnion(msgPostData) });
                            }
                        });
                        newBatch.commit().then((updateRes: any) => {
                            copyThis.mergeFriendAndGroupList().subscribe((getUserResp: any) => {
                                res({
                                    msg: true,
                                    result: getUserResp
                                });
                            }, (error: any) => {
                                rej(error);
                            });
                        }).catch((error: any) => {
                            rej(error);
                        });
                    }).catch((error: any) => {
                        rej(error);
                    });
                }
            }
        });
    }

    public adminAccess(postData: any) {
        return new Promise((res, rej) => {
            this.afs.collection('groupMembers').doc(postData.groupData.groupID).get().subscribe((resp: any) => {
                if (resp && resp.data()?.groupMembers?.length > 0) {
                    const getSelectedUser = resp.data()?.groupMembers.find((ele: any) => ele.uid === postData.chatUser.uid);
                    if (getSelectedUser) {
                        this.afs.collection('groupMembers').doc(postData.groupData.groupID).ref.update({
                            groupMembers: firebase.firestore.FieldValue.arrayRemove(getSelectedUser)
                        }).then((remRes) => {
                            delete getSelectedUser.isAdmin;
                            getSelectedUser.isAdmin = postData.isAdmin;
                            this.afs.collection('groupMembers').doc(postData.groupData.groupID).ref.update({
                                groupMembers: firebase.firestore.FieldValue.arrayUnion(getSelectedUser)
                            }).then((addRes) => {
                                this.afs.collection('messages').doc(postData.groupData.chatId).update({
                                    messages: firebase.firestore.FieldValue.arrayUnion(postData.msgData)
                                }).then((msgRes: any) => {
                                    this.getSingleGroupMembersList(postData.groupData.groupID).subscribe((gpMemRes: any) => {
                                        if (gpMemRes.payload.data() && gpMemRes.payload.data().groupMembers.length > 0) {
                                            res({
                                                msg: true,
                                                result: gpMemRes.payload.data().groupMembers
                                            });
                                        } else {
                                            res({
                                                msg: true,
                                                result: []
                                            });
                                        }
                                    }, (error) => {
                                        rej(error);
                                    });
                                }).catch((error) => {
                                    rej(error);
                                });
                            }).catch((error) => {
                                rej(error);
                            });
                        }).catch((error) => {
                            rej(error);
                        });
                    }
                }
            })
        });
    }

    public exitFromGroup(postData: any) {
        return new Promise((res, rej) => {
            const msgPostData = {
                uid: postData?.removedBy?.uid,
                userName: postData?.removedBy?.displayName,
                content: '',
                dummyMsg: 'Left',
                receiverUid: postData?.currentUser?.uid,
                receiverName: postData?.currentUser?.displayName,
                isFirst: true,
                createdAt: Date.now(),
                isGroup: true,
                isDummy: true,
                isAdminMsg: false,
                isImage: false
            };

            if (postData?.removedBy?.uid === postData?.currentUser?.uid) {
                msgPostData.dummyMsg = 'left';
            } else {
                msgPostData.dummyMsg = 'removed';
                msgPostData.isFirst = false;
            }

            if (postData?.isAdmin === true) {
                this.afs.collection('messages').doc(postData.chatGroupData.chatId).ref.update({
                    messages: firebase.firestore.FieldValue.arrayUnion(msgPostData)
                }).then((msgRes) => {
                    msgPostData.dummyMsg = `You're now an admin`;
                    msgPostData.isFirst = false;
                    msgPostData.isAdminMsg = true;
                    msgPostData.receiverUid = postData?.newAdmin?.uid;
                    msgPostData.receiverName = postData?.newAdmin?.displayName;
                    this.afs.collection('messages').doc(postData.chatGroupData.chatId).ref.update({
                        messages: firebase.firestore.FieldValue.arrayUnion(msgPostData)
                    }).then((msgAddRes) => {
                        this.afs.collection('groups').doc(postData.currentUser.uid).ref.get().then((gRes: any) => {
                            const getGroupData = gRes.data().groups;
                            if (getGroupData?.length > 0) {
                                const selectedGroup = getGroupData.filter((ele: any) => ele.groupID === postData.chatGroupData.groupID);
                                if (selectedGroup?.length > 0) {
                                    this.afs.collection('groups').doc(postData.currentUser.uid).ref.update({
                                        groups: firebase.firestore.FieldValue.arrayRemove(selectedGroup[0])
                                    }).then((remRes: any) => {
                                        const groupMembersQuery = this.afs.collection('groupMembers').doc(postData.chatGroupData.groupID).ref.get();
                                        groupMembersQuery.then((gmRes: any) => {
                                            const groupMembersData = gmRes.data().groupMembers;
                                            if (groupMembersData?.length > 0) {
                                                const selectedGroupMember = groupMembersData.filter((gmEle: any) => gmEle?.uid === postData?.currentUser?.uid);
                                                const nextAdminMember = groupMembersData.filter((gmaEle: any) => gmaEle?.uid === postData?.newAdmin?.uid);
                                                if (selectedGroupMember?.length > 0 && nextAdminMember?.length > 0) {
                                                    this.afs.collection('groupMembers').doc(postData.chatGroupData.groupID).ref.update({
                                                        groupMembers: firebase.firestore.FieldValue.arrayRemove(selectedGroupMember[0])
                                                    }).then((gmRemRes: any) => {
                                                        this.afs.collection('groupMembers').doc(postData.chatGroupData.groupID).ref.update({
                                                            groupMembers: firebase.firestore.FieldValue.arrayRemove(nextAdminMember[0])
                                                        }).then((gmaRemRes: any) => {
                                                            this.afs.collection('groupMembers').doc(postData.chatGroupData.groupID).ref.update({
                                                                groupMembers: firebase.firestore.FieldValue.arrayUnion(postData?.newAdmin)
                                                            }).then((gmaAddRes: any) => {
                                                                this.getSingleGroupMembersList(postData.chatGroupData.groupID).subscribe((gpMemRes: any) => {
                                                                    if (gpMemRes.payload.data() && gpMemRes.payload.data().groupMembers.length > 0) {
                                                                        res({
                                                                            msg: true,
                                                                            result: gpMemRes.payload.data().groupMembers
                                                                        });
                                                                    } else {
                                                                        res({
                                                                            msg: true,
                                                                            result: []
                                                                        });
                                                                    }
                                                                }, (error) => {
                                                                    rej(error);
                                                                });
                                                            }).catch((error) => {
                                                                rej(error);
                                                            });
                                                        }).catch((error) => {
                                                            rej(error);
                                                        });
                                                    }).catch((error) => {
                                                        rej(error);
                                                    });
                                                }
                                            }
                                        }).catch((error) => {
                                            rej(error);
                                        });
                                    }).catch((error) => {
                                        rej(error);
                                    });
                                }
                            }
                        }).catch((error) => {
                            rej(error);
                        });
                    });
                }).catch((error) => {
                    rej(error);
                });
                                                        } else {
                this.afs.collection('messages').doc(postData.chatGroupData.chatId).ref.update({
                    messages: firebase.firestore.FieldValue.arrayUnion(msgPostData)
                }).then((msgRes) => {
                    this.afs.collection('groups').doc(postData.currentUser.uid).ref.get().then((gRes: any) => {
                        const getGroupData = gRes.data().groups;
                        if (getGroupData?.length > 0) {
                            const selectedGroup = getGroupData.filter((ele: any) => ele.groupID === postData.chatGroupData.groupID);
                            if (selectedGroup?.length > 0) {
                                this.afs.collection('groups').doc(postData.currentUser.uid).ref.update({
                                    groups: firebase.firestore.FieldValue.arrayRemove(selectedGroup[0])
                                }).then((remRes: any) => {
                                    const groupMembersQuery = this.afs.collection('groupMembers').doc(postData.chatGroupData.groupID).ref.get();
                                    groupMembersQuery.then((gmRes: any) => {
                                        const groupMembersData = gmRes.data().groupMembers;
                                        if (groupMembersData?.length > 0) {
                                            const selectedGroupMember = groupMembersData.filter((gmEle: any) => gmEle.uid === postData.currentUser.uid);
                                            if (selectedGroupMember?.length > 0) {
                                                this.afs.collection('groupMembers').doc(postData.chatGroupData.groupID).ref.update({
                                                    groupMembers: firebase.firestore.FieldValue.arrayRemove(selectedGroupMember[0])
                                                }).then((gmRemRes: any) => {
                                                    this.getSingleGroupMembersList(postData.chatGroupData.groupID).subscribe((gpMemRes: any) => {
                                                        if (gpMemRes.payload.data() && gpMemRes.payload.data().groupMembers.length > 0) {
                                                            res({
                                                                msg: true,
                                                                result: gpMemRes.payload.data().groupMembers
                                                            });
                                                        } else {
                                                            res({
                                                                msg: true,
                                                                result: []
                                                            });
                                                        }
                                                    }, (error) => {
                                                        rej(error);
                                                    });
                                                }).catch((error) => {
                                                    rej(error);
                                                });
                                            }
                                        }
                                    }).catch((error) => {
                                        rej(error);
                                    });
                                }).catch((error) => {
                                    rej(error);
                                });
                            }
                        }
                    }).catch((error) => {
                        rej(error);
                    });
                }).catch((error) => {
                    rej(error);
                });
            }
        });
    }

    public addNewGroupMessage(postData: any) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
        let getChatId = localStorage.getItem('chatID');
        return this.afs.collection('messages').doc(getChatId).update({
            messages: firebase.firestore.FieldValue.arrayUnion(postData)
        });
    }

    public getMessages(getChatId: any) {
        this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
        let chatId = getChatId ? getChatId : localStorage.getItem('chatID');
        if (chatId) {
            return this.afs.collection('messages', ref => ref.orderBy('createdAt')).doc(chatId).snapshotChanges().pipe(
                map((ele: any) => {
                    const msg = ele.payload.data().messages;
                    msg.forEach((msgEle: any) => {
                        if (msgEle.isDummy) {
                            let getDummyMsg = '';
                            if (msgEle.isFirst) {
                                if (msgEle.uid === this.getCurrentUser?.uid) {
                                    getDummyMsg = `You ${msgEle.dummyMsg}`;
                                } else {
                                    getDummyMsg = `${msgEle.userName} ${msgEle.dummyMsg}`;
                                }
                            } else if (msgEle.isAdminMsg) {
                                getDummyMsg = `${msgEle.dummyMsg}`;
                            } else {
                                if (msgEle.uid === this.getCurrentUser?.uid) {
                                    let senderName = (msgEle.uid === this.getCurrentUser?.uid) ? 'You' : msgEle.userName;
                                    let receiverName = (msgEle.receiverUid === this.getCurrentUser?.uid) ? 'you' : msgEle.receiverName;
                                    getDummyMsg = `${senderName} ${msgEle.dummyMsg} ${receiverName}`;
                                } else if (msgEle.receiverUid === this.getCurrentUser?.uid) {
                                    let senderName = (msgEle.uid === this.getCurrentUser?.uid) ? 'you' : msgEle.userName;
                                    let receiverName = (msgEle.receiverUid === this.getCurrentUser?.uid) ? 'You' : msgEle.receiverName;
                                    getDummyMsg = `${senderName} ${msgEle.dummyMsg} ${receiverName}`;
                                } else {
                                    let senderName = (msgEle.uid === this.getCurrentUser?.uid) ? 'you' : msgEle.userName;
                                    let receiverName = (msgEle.receiverUid === this.getCurrentUser?.uid) ? 'You' : msgEle.receiverName;
                                    getDummyMsg = `${senderName} ${msgEle.dummyMsg} ${receiverName}`;
                                }
                            }
                            msgEle.dummyMsg = getDummyMsg;
                        }
                    });
                    return msg;
                })
            );
        }
    }

    // #region Common Functions Starts Here

    public setAllUserMethod() {
        this.getAllUser().subscribe((res: any) => {
            console.log("get all users from set all users list ===> ", res);
            if (res) {
                this.getCurrentUser = JSON.parse(localStorage.getItem('currentUser'));
                res = res.filter((ele: any) => ele.uid !== this.getCurrentUser?.uid).map((el: any) => {
                    const newData = {
                        uid: el.uid,
                        displayName: el.displayName,
                        aboutStatus: el.aboutStatus,
                        email: el.email,
                        photoURL: el.photoURL
                    };
                    return newData;
                }).sort((a: any, b: any) => {
                    return (a.displayName > b.displayName) ? 1 : ((b.displayName > a.displayName) ? -1 : 0);
                });

                if (res?.length > 0) {
                    this.allUserList.next(res);
                }
            }
        });
    }

    public getAllUserMethod() {
        return this.allUserList.asObservable();
    }

    public setSearchText(data: any) {
        this.searchText.next(data);
    }

    public getSearchText() {
        return this.searchText.asObservable();
    }

    public setProfileSearch(value: boolean) {
        this.isSearch.next(value);
    }

    public getProfileSearch() {
        return this.isSearch.asObservable();
    }

    public setGroupList(value: any) {
        this.setGroupListData.next(value);
    }

    public getGroupList() {
        return this.setGroupListData.asObservable();
    }

    // #endregion Common Functions Ends Here

    // #region Video call Peerjs Section Starts Here

    // public createPeerConnection(userId: string) {
    //     this._peer = new Peer(userId);
    //     // localStorage.setItem('peerConnection', circular.stringify(this._peer));
    // }

    // public init(myEl: HTMLMediaElement, otherEl: HTMLMediaElement, onCalling: Function) {
    //     console.log(onCalling)
    //     this.myEl = myEl;
    //     this.otherEl = otherEl;
    //     this.onCalling = onCalling;
    //     this.answerCall();
    //     this.getOwnMedia();
    // }

    // public call(otherUserId: string) {
    //     const callData: Peer.MediaConnection = this._peer.call(otherUserId, this._localStream);
    //     this.getCallMedia(callData);
    // }

    // public answerCall() {
    //     this._peer.on('call', (call) => {
    //         console.log('Incoming Call');
    //         this.newCall = true;
    //         call.answer(this._localStream);
    //         this.getCallMedia(call);
    //     });

    //     this._peer.on('error', (err) => {
    //         if (this.onCalling) {
    //             this.onCalling();
    //         }
    //     });
    // }

    // public endCall() {
    //     this._existingCall.close();
    //     if (this.onCalling) {
    //         this.onCalling();
    //     }
    // }

    // public getOwnMedia() {
    //     navigator.getUserMedia({ audio: true, video: true }, (stream) => {
    //         this.myEl.srcObject = stream;
    //         this._localStream = stream;
    //         if (this.onCalling) {
    //             this.onCalling();
    //         }
    //     }, (error) => {
    //         console.log(error);
    //     });
    // }

    // public getCallMedia(call: Peer.MediaConnection) {
    //     if (this._existingCall) {
    //         this._existingCall.close();
    //     }

    //     call.on('stream', (stream: MediaProvider) => {
    //         this.otherEl.srcObject = stream;
    //     });

    //     this._existingCall = call;
    //     call.on('close', () => {
    //         if (this.onCalling) {
    //             this.onCalling();
    //         }
    //     });
    // }

    // #endregion Video call Peerjs Section Ends Here
}
