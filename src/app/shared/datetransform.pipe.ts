import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'datetransform',
    standalone: false
})

export class DatetransformPipe implements PipeTransform {

    transform(value: string) {
        let _value = Number(value);
        let dif = Math.floor(((Date.now() - _value) / 1000) / 86400);
        if (dif < 30) {
            return this.formatDate(value);
        } else {
            let datePipe = new DatePipe("en-US");
            value = datePipe.transform(value, 'dd-MM-yyyy');
            return value;
        }
    }

    public formatDate(time: string) {
        let todayDate = new Date();
        let todayFormatedDate = new Intl.DateTimeFormat('en-GB', { day: '2-digit', month: '2-digit', year: '2-digit' }).format(todayDate);
        let yesterdayDate = todayDate.setDate(todayDate.getDate() - 1);
        let yesterdayFormatedDate = new Intl.DateTimeFormat('en-GB', { day: '2-digit', month: '2-digit', year: '2-digit' }).format(yesterdayDate);
        let date = new Date(time);
        let getTodayTime = date.toLocaleTimeString("en-US", { hour: '2-digit', minute: '2-digit' });
        let formatedDate = new Intl.DateTimeFormat('en-GB', { day: '2-digit', month: '2-digit', year: '2-digit' }).format(date);
        let diff = (((new Date()).getTime() - date.getTime()) / 1000);
        let daydiff = Math.floor(diff / 86400);

        if (isNaN(daydiff) || daydiff < 0 || daydiff >= 31)
            return '';

        return daydiff == 0 && (todayFormatedDate == formatedDate) && getTodayTime ||
            yesterdayFormatedDate == formatedDate && 'Yesterday' ||
            daydiff > 1 && formatedDate || formatedDate;

        // if (daydiff == 0 && (todayFormatedDate == formatedDate)) {
        //     return getTodayTime;
        // } else if (yesterdayFormatedDate == formatedDate) {
        //     return 'Yesterday';
        // } else if (daydiff > 1) {
        //     return formatedDate;
        // } else {
        //     return formatedDate;
        // }
    }
}