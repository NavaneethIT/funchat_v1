import { Component, Input, OnInit, SimpleChanges } from "@angular/core";
import { Router } from '@angular/router';
import { AlertController, LoadingController, PopoverController } from "@ionic/angular";
import { ChatService } from "src/app/services/chat.service";

@Component({
    selector: "app-access-popover",
    template: `
                <ion-list lines="none">
                    <ion-item (click)="iconsClick('chat')">
                        <ion-label>Message {{chatUser.displayName}}</ion-label>
                    </ion-item>

                    <ion-item (click)="iconsClick('view')">
                        <ion-label>View {{chatUser.displayName}}</ion-label>
                    </ion-item>

                    <ion-item (click)="iconsClick('makeAdmin')" *ngIf="isAdmin && !chatUser.isAdmin">
                        <ion-label>Make Group Admin</ion-label>
                    </ion-item>

                    <ion-item (click)="iconsClick('removeAdmin')" *ngIf="isAdmin && chatUser.isAdmin">
                        <ion-label>Dismiss as Admin</ion-label>
                    </ion-item>

                    <ion-item (click)="iconsClick('remove')" *ngIf="isAdmin">
                        <ion-label>Remove {{chatUser.displayName}}</ion-label>
                    </ion-item>
                </ion-list>
            `,
    standalone: false
})

export class AccessPopoverComponent implements OnInit {

    @Input() public chatUser: any;
    @Input() public isAdmin: any;

    constructor(
        private router: Router,
        private chatService: ChatService,
        private popoverCtrl: PopoverController,
        private alertCtrl: AlertController,
        private loadingController: LoadingController,
    ) { }

    ngOnInit() {
    }

    public iconsClick(clickType: string) {
        this.popoverCtrl.dismiss();
        switch(clickType) {
            case 'chat': {
                localStorage.removeItem('chatUser');
                localStorage.removeItem('chatID');
                if (!this.chatUser.hasOwnProperty('isGroup')) {
                    this.chatUser.isGroup = false;
                    localStorage.setItem('chatUser', JSON.stringify(this.chatUser));
                } else {
                    localStorage.setItem('chatUser', JSON.stringify(this.chatUser));
                }
                this.router.navigateByUrl('/chats', { replaceUrl: true });
                break;
            }
            case 'view': {
                localStorage.removeItem('chatUser');
                localStorage.removeItem('chatID');
                if (!this.chatUser.hasOwnProperty('isGroup')) {
                    this.chatUser.isGroup = false;
                    localStorage.setItem('chatUser', JSON.stringify(this.chatUser));
                } else {
                    localStorage.setItem('chatUser', JSON.stringify(this.chatUser));
                }

                localStorage.setItem('viewFrom', JSON.stringify('popup'));
                this.router.navigateByUrl('/commonprofile', { replaceUrl: true });
                break;
            }
            case 'makeAdmin': {
                this.adminAccess(clickType);
                break;
            }
            case 'removeAdmin': {
                this.adminAccess(clickType);
                break;
            }
            case 'remove': {
                this.exitGroupClick();
                break;
            }
        }
    }

    public async adminAccess(adminType: string) {
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        const getChatGroupData = JSON.parse(localStorage.getItem('chatUser'));

        const msgPostData = {
            uid: currentUser?.uid,
            userName: currentUser?.displayName,
            content: '',
            dummyMsg: (adminType === 'makeAdmin') ? `You're now an admin` : `You're no longer an admin`,
            receiverUid: this.chatUser?.uid,
            receiverName: this.chatUser?.displayName,
            isFirst: false,
            createdAt: Date.now(),
            isGroup: true,
            isDummy: true,
            isAdminMsg: true
        };

        const postData = {
            isAdmin: (adminType === 'makeAdmin') ? true : false,
            groupData: getChatGroupData,
            chatUser: this.chatUser,
            msgData: msgPostData
        };

        const loading = await this.loadingController.create({
            message: (adminType === 'makeAdmin') ? 'Adding...' : 'Removing...',
            backdropDismiss: false,
            keyboardClose: false
        });

        loading.present();

        this.chatService.adminAccess(postData).then((res: any) => {
            loading.dismiss();
            if (res?.msg && res?.result?.length > 0) {
                const getCurrenChatUser = JSON.parse(localStorage.getItem('chatUser'));
                localStorage.removeItem('chatUser')
                getCurrenChatUser.groupMembers = [];
                getCurrenChatUser.groupMembers = res?.result;
                localStorage.setItem('chatUser', JSON.stringify(getCurrenChatUser));
                this.router.navigateByUrl('/commonprofile', { replaceUrl: true });
            }
        }).catch((error: any) => {
            loading.dismiss();
            console.error(error);
        });
    }

    public async exitGroupClick() {
        const getChatGroupData = JSON.parse(localStorage.getItem('chatUser'));
        const currentUserInfo = JSON.parse(localStorage.getItem('currentUser'));
        const alert = await this.alertCtrl.create({
            message: `Remove ${this.chatUser.displayName} from ${getChatGroupData?.displayName} group?`,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => { }
                }, {
                    text: 'Exit',
                    handler: async () => {
                        const loading = await this.loadingController.create({
                            message: 'Removing...',
                            backdropDismiss: false,
                            keyboardClose: false,
                        });
                        await loading.present();
                        delete getChatGroupData.groupMembers
                        const groupData = getChatGroupData;
                        delete currentUserInfo.friendsList;
                        const userData = currentUserInfo;
                        const postData = {
                            chatGroupData: groupData,
                            currentUser: this.chatUser,
                            removedBy: userData,
                            isAdmin: false,
                            newAdmin: null
                        };
                
                        this.chatService.exitFromGroup(postData).then((res: any) => {
                            loading.dismiss();
                            if (res?.msg && res?.result?.length > 0) {
                                const getCurrenChatUser = JSON.parse(localStorage.getItem('chatUser'));
                                localStorage.removeItem('chatUser')
                                getCurrenChatUser.groupMembers = [];
                                getCurrenChatUser.groupMembers = res?.result;
                                localStorage.setItem('chatUser', JSON.stringify(getCurrenChatUser));
                                this.router.navigateByUrl('/commonprofile', { replaceUrl: true });
                            }
                        }).catch((error) => {
                            loading.dismiss();
                            console.error(error);
                        });
                    }
                }
            ]
        });
        await alert.present();
    }

}
