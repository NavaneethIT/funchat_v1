import { Component, Input, OnInit, SimpleChanges } from "@angular/core";
import { Router } from '@angular/router';
import { AlertController, PopoverController } from "@ionic/angular";
import { ChatService } from "src/app/services/chat.service";

@Component({
    selector: "app-profile-popover",
    template: `
                <ion-list lines="none" *ngIf="isViewMyProfile">
                    <ion-item (click)="createNewGroupClick()">
                        <ion-label>New Group</ion-label>
                    </ion-item>

                    <ion-item (click)="viewProfileClick()">
                        <ion-label>View Profile</ion-label>
                    </ion-item>

                    <ion-item style="display: none !important;" (click)="signOutClick()">
                        <ion-label>Sign Out</ion-label>
                        <ion-icon name="log-out-outline"></ion-icon>
                    </ion-item>
                </ion-list>

                <ion-list class="groupPersonalProfileClass" lines="none" *ngIf="!isViewMyProfile && isViewProfile">
                    <ion-item (click)="viewContact()">
                        <ion-label>View Contact</ion-label>
                    </ion-item>

                    <ion-item (click)="searchClick()">
                        <ion-label>Search</ion-label>
                    </ion-item>
                </ion-list>

                <ion-list lines="none" *ngIf="!isViewMyProfile && isViewGroupProfile">
                    <ion-item (click)="viewContact()">
                        <ion-label>Group Info</ion-label>
                    </ion-item>

                    <ion-item (click)="searchClick()">
                        <ion-label>Search</ion-label>
                    </ion-item>
                </ion-list>
            `,
    standalone: false
})

export class ProfilePopoverComponent implements OnInit {

    @Input() public chatUser: any;

    public isViewMyProfile: boolean = false;
    public isViewProfile: boolean = false;
    public isViewGroupProfile: boolean = false;

    constructor(
        private router: Router,
        private chatService: ChatService,
        private popoverCtrl: PopoverController,
        private alertCtrl: AlertController
    ) { }

    ngOnInit() {
        this.isViewMyProfile = this.chatUser ? false : true;
        this.isViewProfile = this.chatUser?.isGroup ? false : true;
        this.isViewGroupProfile = this.chatUser?.isGroup ? true : false;
    }

    public viewContact() {
        this.popoverCtrl.dismiss();
        localStorage.setItem('viewFrom', JSON.stringify('page'));
        this.router.navigateByUrl('/commonprofile', { replaceUrl: true });
    }

    public searchClick() {
        this.popoverCtrl.dismiss();
        this.chatService.setProfileSearch(true);
    }

    public createNewGroupClick() {
        this.popoverCtrl.dismiss();
        this.router.navigateByUrl('/creategroup', { replaceUrl: true });
    }

    public viewProfileClick() {
        this.popoverCtrl.dismiss();
        this.router.navigateByUrl('/profile', { replaceUrl: true });
    }

    public async signOutClick() {
        this.popoverCtrl.dismiss();
        const alert = await this.alertCtrl.create({
            header: 'Sign Out',
            message: 'Do you want to Sign Out ?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => { }
                }, {
                    text: 'Yes',
                    handler: () => {
                        this.chatService.signOut().then((res: any) => {
                            localStorage.removeItem('currentUser');
                            localStorage.removeItem('chatUser');
                            localStorage.removeItem('chatID');
                            localStorage.removeItem('groupList');
                            this.router.navigateByUrl('/signin', { replaceUrl: true });
                        }, async (error) => {
                            const errorAlert = await this.alertCtrl.create({
                                header: 'Sign Out Failed',
                                message: error.message
                            });
                            await errorAlert.present();
                        });
                    }
                }
            ]
        });
        await alert.present();
    }
}
