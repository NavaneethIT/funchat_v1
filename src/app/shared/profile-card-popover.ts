import { Component, Input, OnInit, SimpleChanges } from "@angular/core";
import { Router } from '@angular/router';
import { AlertController, PopoverController } from "@ionic/angular";
import { ChatService } from "src/app/services/chat.service";

@Component({
    selector: "app-profileCard-popover",
    template: `
                <ion-card>
                    <p class="profile-card-name">{{chatUser.displayName}}</p>
                    <img class="profile-card-img" [src]="photoUrl">
                    <div class="profile-card-icons" *ngIf="!isGroup">
                        <ion-icon name="chatbox-ellipses" (click)="iconClicks('chat')"></ion-icon>
                        <ion-icon name="call"></ion-icon>
                        <ion-icon name="videocam"></ion-icon>
                        <ion-icon name="information-circle-outline" (click)="iconClicks('info')"></ion-icon>
                    </div>

                    <div class="profile-card-group-icons" *ngIf="isGroup">
                        <ion-icon name="chatbox-ellipses" (click)="iconClicks('chat')"></ion-icon>
                        <ion-icon name="information-circle-outline" (click)="iconClicks('info')"></ion-icon>
                    </div>
                </ion-card>
            `,
    standalone: false
})

export class ProfileCardPopoverComponent implements OnInit {

    @Input() public chatUser: any;

    public photoUrl: string = '';
    public isGroup: boolean = true;
    public currentUser = JSON.parse(localStorage.getItem('currentUser'));

    constructor(
        private router: Router,
        private chatService: ChatService,
        private popoverCtrl: PopoverController
    ) { }

    ngOnInit() {
        this.isGroup = this.chatUser?.isGroup;
        if (this.chatUser?.isGroup) {
            this.photoUrl = (
                this.chatUser.photoURL !== '' &&
                this.chatUser.photoURL !== null &&
                this.chatUser.photoURL !== 'null' &&
                this.chatUser.photoURL !== undefined
            ) ? this.chatUser.photoURL : '../../../assets/icon/profile_group.png';
        } else {
            this.photoUrl = (
                this.chatUser.photoURL !== '' &&
                this.chatUser.photoURL !== null &&
                this.chatUser.photoURL !== 'null' &&
                this.chatUser.photoURL !== undefined
            ) ? this.chatUser.photoURL : '../../../assets/icon/profile_user.png';
        }
    }

    public iconClicks(clickType: string) {
        this.popoverCtrl.dismiss();
        localStorage.removeItem('chatUser');
        localStorage.removeItem('chatID');
        if (!this.chatUser.hasOwnProperty('isGroup')) {
            this.chatUser.isGroup = false;
            localStorage.setItem('chatUser', JSON.stringify(this.chatUser));
            const getCurrentChatData = this.currentUser?.friendsList.find((ele: any) => ele.uid === this.chatUser.uid);
            if (getCurrentChatData && getCurrentChatData?.chatId) {
                localStorage.removeItem('chatUser');
                localStorage.setItem('chatUser', JSON.stringify(getCurrentChatData));
                localStorage.setItem('chatID', getCurrentChatData.chatId);
            } else {
                localStorage.removeItem('chatID');
            }
        } else {
            localStorage.setItem('chatUser', JSON.stringify(this.chatUser));
            localStorage.setItem('chatID', this.chatUser.chatId);
        }

        switch(clickType) {
            case 'chat': {
                this.router.navigateByUrl('/chats', { replaceUrl: true });
                break;
            }
            case 'info': {
                localStorage.setItem('viewFrom', JSON.stringify('popup'));
                this.router.navigateByUrl('/commonprofile', { replaceUrl: true });
                break;
            }
        }
    }
}

